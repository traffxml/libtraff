[![Release](https://jitpack.io/v/org.traffxml/libtraff.svg)](https://jitpack.io/#org.traffxml/libtraff)

The Traffic Feed Format (TraFF) library provides an object model for TraFF. It implements the [TraFF specification](https://gitlab.com/traffxml/traff) and offers the following features:

* Builders for the more complex classes, through which individual members can be set one-by-one, and which validate the resulting object before building it.
* XML output of a TraFF feed or a single object.
* Planned: XML input parsing

# Branches

This repository has two branches:

* `master` always holds the latest released version. Beginning with version 0.8, it always implements the corresponding version of the specification.
* `dev` holds the version currently under development. Once it is ready for publication, it will get merged into `master`.

This convention is currently being introduced across all TraFF projects.

# Javadoc

Javadoc for `master` is at https://traffxml.gitlab.io/libtraff/javadoc/master/.

Javadoc for `dev` is at https://traffxml.gitlab.io/libtraff/javadoc/dev/.
