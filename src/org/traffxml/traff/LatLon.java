/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff;

import java.text.ParseException;

/**
 * Encodes a WGS84 coordinate pair.
 */
public class LatLon {
	/** Earth radius in km (mean earth radius according to WGS84). */
	private static final double EARTH_RADIUS = 6371.0088f;

	/** Earth circumference in km. */
	public static final float EARTH_CIRCUMFERENCE = (float) (2 * EARTH_RADIUS * Math.PI);

	/** Radians per degree. */
	private static final double RAD_PER_DEG = Math.PI / 180.0f;

	/** Latitude in degrees. */
	public final float lat;
	/** Longitude in degrees. */
	public final float lon;

	/**
	 * Creates a new coordinate pair.
	 * 
	 * @param lat The latitude, in degrees
	 * @param lon The longitude, in degrees
	 * 
	 * @throws IllegalArgumentException if latitude or longitude are out of range
	 * (+/-90 and +/-180 degrees, respectively)
	 */
	public LatLon(float lat, float lon) {
		if ((Math.abs(lat) > 90.0f) || (Math.abs(lon) > 180.0f))
			throw new IllegalArgumentException();
		this.lat = lat;
		this.lon = lon;
	}

	/**
	 * Creates a new coordinate pair from a string.
	 * 
	 * <p>The string must contain latitude and longitude (in that order), separated by one whitespace character.
	 * This is the format commonly used in XML.
	 * 
	 * @param coordinates
	 * @throws ParseException if {@code coordinates} does not contain a valid coordinate pair
	 * @throws IllegalArgumentException if latitude or longitude are out of range
	 * (+/-90 and +/-180 degrees, respectively)
	 */
	public LatLon(String coordinates) throws ParseException {
		String[] individualCoordinates = coordinates.split("\\s");
		if (individualCoordinates.length < 2)
			throw new ParseException(coordinates, 0);
		lat = Float.valueOf(individualCoordinates[0]);
		lon = Float.valueOf(individualCoordinates[1]);
		if ((Math.abs(lat) > 90.0f) || (Math.abs(lon) > 180.0f))
			throw new IllegalArgumentException();
	}

	/**
	 * Calculates the bearing from the coordinate pair to another.
	 * 
	 * <p>BUGS: To this method, the earth is (nearly) a rectangle. Anything that crosses the 180 degree meridian
	 * or the poles will give incorrect results. Bearings will be inaccurate as distance from the equator
	 * and distance between the two points increases.
	 * 
	 * <p>This method is only useful for approximate bearings, such as in indicating a cardinal direction. If
	 * you need accurate bearings, use a proper purpose-built library such as Proj4J.
	 * 
	 * @param that The second coordinate pair
	 * @return Bearing from {@code this} to {@code that}, in degrees and normalized to [0, 360) degrees.
	 */
	public double bearingTo(LatLon that) {
		float meanLat = (this.lat + that.lat) / 2;
		double lonFactor = Math.cos(Math.toRadians(meanLat));
		double lonDiff = (that.lon - this.lon) * lonFactor;
		float latDiff = that.lat - this.lat;
		double res = Math.toDegrees(Math.atan2(lonDiff, latDiff));
		if (res < 0)
			res += 360;
		return res;
	}

	/**
	 * Calculates the distance between two coordinate pairs.
	 * 
	 * <p>BUGS: To this method, the earth is a perfect sphere, thus results may have an error of up to 0.5%.
	 * 
	 * @param that The second coordinate pair
	 * @return Distance in km
	 */
	public double distanceTo(LatLon that) {
		return EARTH_RADIUS * haversine(this.lon * RAD_PER_DEG, this.lat * RAD_PER_DEG, that.lon * RAD_PER_DEG, that.lat * RAD_PER_DEG);
	}

	/**
	 * Whether two {@code LatLon} instances are identical.
	 * 
	 * @param that
	 */
	public boolean equals(LatLon that) {
		return (this.lat == that.lat) && (this.lon == that.lon);
	}

	@Override
	public int hashCode() {
		/*
		 * First normalize lat and lon to 32 bits. Remaining decimals are truncated, resulting in a
		 * resolution of approx. 0.5 mm for latitude. For longitude the resolution is approx. 1 mm at the
		 * equator and 0.7 mm at 45°.
		 * Next, swap the 16-bit halves of normLon, and XOR the result with normLat. Swapping the halves of
		 * one member helps prevent systematic variations to both components from canceling each other out.
		 */
		int normLat = (int)(lat * 23855104);
		int normLon = (int)(lon * 11927552);
		return normLat ^ (normLon >>> 16) ^ (normLon << 16);
	}

	/**
	 * Calculates the great-circle distance between two points on a sphere.
	 * 
	 * @param lon1 Longitude of the first point in radians
	 * @param lat1 Latitude of the first point in radians
	 * @param lon2 Longitude of the second point in radians
	 * @param lat2 Latitude of the second point in radians
	 * 
	 * @return Great-circle distance in radians (multiply with the sphere radius to get the distance on the sphere surface)
	 */
	/* From proj4j */
	private static double haversine(double lon1, double lat1, double lon2, double lat2) {
		double dlat = Math.sin((lat2 - lat1) / 2);
		double dlon = Math.sin((lon2 - lon1) / 2);
		double r = Math.sqrt(dlat * dlat + Math.cos(lat1) * Math.cos(lat2) * dlon * dlon);
		return 2.0 * Math.asin(r);
	}
}
