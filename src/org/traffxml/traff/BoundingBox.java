/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff;

import java.text.ParseException;
import java.util.Collection;

/**
 * Encodes an area enclosed by a WGS84 latitude and longitude range.
 * 
 * <p>It is legal for the latitude and/or longitude boundaries of a BoundingBox to coincide.
 * 
 * <p>A BoundingBox cannot extend beyond a pole. In order to denote an area around a pole, use that pole and
 * the latitude farthest from it as its latitude boundaries.
 * 
 * <p>A BoundingBox can extend beyond the 180 degree meridian (i.e. {@code minLon > maxLon}). The only way to
 * represent a BoundingBox which spans the entire globe (possibly limited by latitudes) is to set its
 * {@code minLon} to -180 and its {@code maxLon} to 180: any other case in which these two coincide
 * would describe a BoundingBox with zero longitudinal extension.
 * 
 * <p>Convenience methods are available to construct a BoundingBox which encloses a group of coordinate
 * pairs, or to extend a BoundingBox to include a coordinate pair or another BoundingBox. All of these are
 * intended for BoundingBoxes which span less than 180 degrees in longitude. While it is guaranteed that
 * every such operation will result in a BoundingBox which encloses all operands, results may be unexpected
 * if the operands cover a longitude range of more than 180 degrees.
 */
public class BoundingBox {
	/** Minimum latitude in degrees. */
	public final float minLat;
	/** Minimum longitude in degrees. */
	public final float minLon;
	/** Maximum latitude in degrees. */
	public final float maxLat;
	/** Maximum longitude in degrees. */
	public final float maxLon;

	/**
	 * Creates a new BoundingBox.
	 * 
	 * @param minLat The minimum (southern) latitude, in degrees
	 * @param minLon The minimum (western) longitude, in degrees
	 * @param maxLat The maximum (northern) latitude, in degrees
	 * @param maxLon The maximum (eastern) longitude, in degrees
	 * 
	 * @throws IllegalArgumentException if any latitude or longitude is out of range
	 * (+/-90 and +/-180 degrees, respectively), or if the minimum latitude is greater than the maximum
	 * latitude.
	 */
	public BoundingBox(float minLat, float minLon, float maxLat, float maxLon) {
		if ((Math.abs(minLat) > 90.0f) || (Math.abs(maxLat) > 90.0f)
				|| (Math.abs(minLon) > 180.0f) || (Math.abs(maxLon) > 180.0f)
				|| (minLat > maxLat))
			throw new IllegalArgumentException(String.format("minLat = %f, maxLat = %f, minLon = %f, maxLon = %f",
					minLat, minLon, maxLat, maxLon));
		this.minLat = minLat;
		this.minLon = minLon;
		this.maxLat = maxLat;
		this.maxLon = maxLon;
	}

	/**
	 * Creates a new BoundingBox from a single coordinate pair.
	 * 
	 * <p>The result is a BoundingBox whose boundaries in either dimension coincide.
	 * 
	 * @param coords The coordinate pair
	 */
	public BoundingBox(LatLon coords) {
		this(coords.lat, coords.lon, coords.lat, coords.lon);
	}

	/**
	 * Creates a new BoundingBox from a collection of coordinate pairs.
	 * 
	 * <p>The result is a BoundingBox which contains all coordinate pairs in the collection.
	 * 
	 * <p>This will not work well if the resulting BoundingBox spans a longitude range of 180 degrees
	 * or more, as the order of coordinate pairs in the collection may affect results.
	 * 
	 * @param coordPairs The coordinate pairs
	 */
	public BoundingBox(Collection<LatLon> coordPairs) {
		if (coordPairs.isEmpty())
			throw new IllegalArgumentException();
		BoundingBox tmp = null;
		for (LatLon coords: coordPairs)
			if (tmp == null)
				tmp = new BoundingBox(coords);
			else
				tmp = tmp.extend(coords);
		minLat = tmp.minLat;
		minLon = tmp.minLon;
		maxLat = tmp.maxLat;
		maxLon = tmp.maxLon;
	}

	/**
	 * Creates a new BoundingBox from a string.
	 * 
	 * <p>The string must contain minimum latitude, minimum longitude, maximum latitude and maximum
	 * longitude (in that order), separated by one whitespace character.
	 * This is the format commonly used in XML.
	 * 
	 * @param coordinates
	 * @throws ParseException if {@code coordinates} does not contain two valid coordinate pairs
	 * @throws IllegalArgumentException if latitude or longitude are out of range
	 * (+/-90 and +/-180 degrees, respectively) or the minimum latitude exceeds the maximum one
	 */
	public BoundingBox(String coordinates) throws ParseException {
		String[] individualCoordinates = coordinates.split("\\s");
		if (individualCoordinates.length < 4)
			throw new ParseException(coordinates, 0);
		minLat = Float.valueOf(individualCoordinates[0]);
		minLon = Float.valueOf(individualCoordinates[1]);
		maxLat = Float.valueOf(individualCoordinates[2]);
		maxLon = Float.valueOf(individualCoordinates[3]);
		if ((Math.abs(minLat) > 90.0f) || (Math.abs(maxLat) > 90.0f)
				|| (Math.abs(minLon) > 180.0f) || (Math.abs(maxLon) > 180.0f)
				|| (minLat > maxLat))
			throw new IllegalArgumentException(String.format("minLat = %f, maxLat = %f, minLon = %f, maxLon = %f",
					minLat, minLon, maxLat, maxLon));
	}

	/**
	 * Whether the bounding box contains the specified coordinates.
	 * 
	 * @param coordinates
	 * @return True if {@code coordinates} is inside the bounding box (including on its border), false otherwise. 
	 */
	public boolean contains(LatLon coordinates) {
		if ((coordinates.lat < this.minLat) || (coordinates.lat > this.maxLat))
			return false;
		if (this.minLon <= this.maxLon)
			return (coordinates.lon >= this.minLon) && (coordinates.lon <= this.maxLon);
		else
			return (coordinates.lon <= this.minLon) || (coordinates.lon >= this.maxLon);
	}

	/**
	 * Extends this BoundingBox to include a given coordinate pair and returns the result.
	 * 
	 * <p>The original BoundingBox is not modified.
	 * 
	 * <p>This will not work well if the resulting BoundingBox spans a longitude range of 180 degrees
	 * or more: the result of multiple extend operations may be affected by the order in which the
	 * operands are applied.
	 * 
	 * @param coords The coordinate pair by which to extend the BoundingBox.
	 * 
	 * @return The smallest BoundingBox encompassing the original BoundingBox and the new coordinate pair.
	 */
	public BoundingBox extend(LatLon coords) {
		return merge(new BoundingBox(coords));
	}

	@Override
	public int hashCode() {
		/*
		 * First normalize each lat and lon to 32 bits. Remaining decimals are truncated, resulting in a
		 * resolution of approx. 0.5 mm for latitude. For longitude the resolution is approx. 1 mm at the
		 * equator and 0.7 mm at 45°.
		 * Next, rotate each by a different number of bits (0, 8, 16 and 24) and XOR the numbers. The
		 * rotation helps prevent systematic variations to multiple components from canceling each other out.
		 */
		int normMinLat = (int)(minLat * 23855104);
		int normMaxLat = (int)(maxLat * 23855104);
		int normMinLon = (int)(minLon * 11927552);
		int normMaxLon = (int)(maxLon * 11927552);
		return normMinLat ^ (normMinLon >>> 16) ^ (normMinLon << 16)
				^ (normMaxLat >>> 8) ^ (normMaxLat << 24) ^ (normMaxLon >>> 24) ^ (normMaxLon << 8);
	}

	/**
	 * Merges this BoundingBox with another and returns the result.
	 * 
	 * <p>Neither of the two original BoundingBoxes is modified.
	 * 
	 * <p>This will not work well if the resulting BoundingBox spans a longitude range of 180 degrees
	 * or more: the result of multiple extend operations may be affected by the order in which the
	 * operands are applied.
	 * 
	 * @param that The other BoundingBox
	 * 
	 * @return The smallest BoundingBox encompassing both BoundingBoxes.
	 */
	public BoundingBox merge(BoundingBox that) {
		float newMinLon;
		float newMaxLon;
		if ((this.minLon == this.maxLon) && (that.minLon == that.maxLon)) {
			/* Neither has a longitudinal extension */
			if (Math.abs(this.minLon - that.minLon) <= 180.0f) {
				newMinLon = Math.min(this.minLon, that.minLon);
				newMaxLon = Math.max(this.maxLon, that.maxLon);
			} else {
				newMinLon = Math.max(this.maxLon, that.maxLon);
				newMaxLon = Math.min(this.minLon, that.minLon);
			}
		} else if ((this.minLon <= this.maxLon) && (that.minLon <= that.maxLon)) {
			/* Neither crosses the 180 degree meridian */
			if ((this.minLon > that.maxLon) || (this.maxLon < that.minLon)) {
				/* No overlap */
				if (lonDiff(this.maxLon, that.minLon) <= lonDiff(that.maxLon, this.minLon)) {
					newMinLon = this.minLon;
					newMaxLon = that.maxLon;
				} else {
					newMinLon = that.minLon;
					newMaxLon = this.maxLon;
				}
			} else {
				/* overlap, use absolute min and max */
				newMinLon = Math.min(this.minLon, that.minLon);
				newMaxLon = Math.max(this.maxLon, that.maxLon);
			}
		} else if ((this.minLon > this.maxLon) && (that.minLon > that.maxLon)) {
			/* Both cross the 180 degree meridian */
			newMinLon = Math.min(this.minLon, that.minLon);
			newMaxLon = Math.max(this.maxLon, that.maxLon);
			if (newMinLon <= newMaxLon) {
				/* Each covers the “gap” in the other, thus the result spans the whole globe */
				newMinLon = -180.0f;
				newMaxLon = 180.0f;
			}
		} else {
			/* One crosses the 180 degree meridian, the other does not */
			if ((this.minLon <= that.maxLon) && (that.minLon <= this.maxLon)) {
				/* One covers the “gap” in the other, thus the result spans the whole globe */
				newMinLon = -180.0f;
				newMaxLon = 180.0f;
			} else if ((this.minLon > that.maxLon) && (this.maxLon < that.minLon)) {
				/* No overlap */
				if (lonDiff(this.maxLon, that.minLon) <= lonDiff(that.maxLon, this.minLon)) {
					newMinLon = this.minLon;
					newMaxLon = that.maxLon;
				} else {
					newMinLon = that.minLon;
					newMaxLon = this.maxLon;
				}
			} else {
				/* Overlap on one boundary, but gap remains on the other */
				/* 
				 * inner is the bbox which remains within the +/-180 range,
				 * outer is the one which wraps around 
				 */
				BoundingBox inner, outer;
				if (this.minLon <= this.maxLon) {
					inner = this;
					outer = that;
				} else {
					inner = that;
					outer = this;
				}
				if (outer.maxLon < inner.minLon) {
					newMinLon = inner.minLon;
					newMaxLon = outer.maxLon;
				} else {
					newMinLon = outer.minLon;
					newMaxLon = inner.maxLon;
				}
			}
		}
		return new BoundingBox(Math.min(this.minLat, that.minLat), newMinLon, Math.max(this.maxLat, that.maxLat), newMaxLon);
	}

	/**
	 * Whether this BoundingBox overlaps with another BoundingBox, i.e.<!-- --> has at least one point in common.
	 * 
	 * @param that The BoundingBox to compare against
	 * 
	 * @return True if both BoundingBoxes have at least one point in common, false if not
	 */
	public boolean overlaps(BoundingBox that) {
		/* Special case: both bboxes share a pole */
		if (((this.minLat == -90.0f) && (that.minLat == -90.0f))
				|| ((this.maxLat == 90.0f) && (that.maxLat == 90.0f)))
			return true;

		/* 
		 * If the minimum latitude of one bbox is greater than the maximum latitude of the other,
		 * they cannot overlap.
		 * Otherwise, all further tests only need to examine the longitudes.
		 */
		if ((this.minLat > that.maxLat) || (this.maxLat < that.minLat))
			return false;

		/* Special case: both bboxes touch the 180 degree meridian, thus they overlap. */
		if (((Math.abs(this.minLon) == 180.0f) || (Math.abs(this.maxLon) == 180.0f))
				&& ((Math.abs(that.minLon) == 180.0f) || (Math.abs(that.maxLon) == 180.0f)))
			return true;

		/* General case */
		if ((this.minLon <= this.maxLon) && (that.minLon <= that.maxLon)) {
			/* 
			 * Neither bbox extends beyond the 180 degree meridian.
			 * If the minimum longitude of one is greater than the maximum longitude of the other,
			 * the two do not overlap.
			 */
			if ((this.minLon > that.maxLon) || (this.maxLon < that.minLon))
				return false;
			else
				return true;
		} else if ((this.minLon > this.maxLon) && (that.minLon > that.maxLon)) {
			/* Both bboxes extend beyond the 180 degree meridian, thus they overlap at least there. */
			return true;
		} else {
			/* 
			 * One bbox extends beyond the 180 degree meridian, the other does not.
			 * If the longitude boundaries of one fall entirely within the “gap” of the other
			 * (excluding the boundaries), the two do not overlap. Conveniently, this operation is
			 * perfectly commutative.
			 */
			if ((this.minLon > that.maxLon) && (this.maxLon < that.minLon))
				return false;
			else
				return true;
		}
	}

	/**
	 * Calculates the difference between two longitudes.
	 * 
	 * @param lon1
	 * @param lon2
	 * 
	 * @return The absolute difference in degrees, a value between 0 and 180.
	 */
	private static float lonDiff(float lon1, float lon2) {
		float res = Math.abs(lon1 - lon2);
		if (res > 180.0f)
			res = 360.0f - res;
		return res;
	}
}
