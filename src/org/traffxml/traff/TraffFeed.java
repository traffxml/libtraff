/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * Encapsulates a TraFF feed.
 * 
 * <p>A feed is a collection of messages delivered together.
 */
@Root(strict=false, name="feed")
public class TraffFeed {
	private List<TraffMessage> messages = new ArrayList<TraffMessage>();

	/**
	 * Reads an XML input stream and returns the resulting {@code TraffFeed}.
	 * 
	 * @param inputStream An input stream which holds a valid TraFF feed
	 * 
	 * @return An {@code TraffFeed} representing the data in the input stream
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static TraffFeed read(InputStream inputStream) throws Exception {
		TraffFeed res = Traff.XML_SERIALIZER.read(TraffFeed.class, inputStream);
		return res;
	}

	/**
	 * Reads an XML string and returns the resulting {@code TraffFeed}.
	 * 
	 * <p>This is a convenience wrapper around {@link #read(InputStream)}.
	 * 
	 * @param xml A string which holds a valid TraFF feed
	 * 
	 * @return An {@code TraffFeed} representing the data in the string
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static TraffFeed read(String xml) throws Exception {
		return read(new ByteArrayInputStream(xml.getBytes()));
	}

	public TraffFeed() {
		super();
	}

	public TraffFeed(@ElementList(inline=true) List<TraffMessage> messages) {
		super();
		this.messages.addAll(messages);
	}

	@ElementList(inline=true, required=false)
	public List<TraffMessage> getMessages() {
		return messages;
	}

	/**
	 * Returns the feed as a string in TraFF XML format.
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public String toXml() throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		write(out);
		return out.toString();
	}

	/**
	 * Writes a feed to an XML stream.
	 * 
	 * @param out The stream to which the XML output will be written
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public void write(OutputStream out) throws Exception {
		Traff.XML_SERIALIZER.write(this, out);
	}
}
