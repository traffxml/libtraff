/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;
import org.simpleframework.xml.strategy.TreeStrategy;
import org.simpleframework.xml.transform.RegistryMatcher;
import org.traffxml.traff.util.FallbackEnumTransform;
import org.traffxml.traff.util.NullEnumTransform;

public class Traff {
	static final String INDENT_STR = "  ";
	static final int INDENT = INDENT_STR.length();
	static final RegistryMatcher XML_MATCHER = new RegistryMatcher();
	
	static {
		XML_MATCHER.bind(TraffMessage.Urgency.class, new NullEnumTransform(TraffMessage.Urgency.class));
		XML_MATCHER.bind(TraffEvent.Class.class, new FallbackEnumTransform(TraffEvent.Class.class));
		XML_MATCHER.bind(TraffEvent.Type.class, new FallbackEnumTransform(TraffEvent.Type.class));
		XML_MATCHER.bind(TraffSupplementaryInfo.Class.class, new FallbackEnumTransform(TraffSupplementaryInfo.Class.class));
		XML_MATCHER.bind(TraffSupplementaryInfo.Type.class, new FallbackEnumTransform(TraffSupplementaryInfo.Type.class));
		XML_MATCHER.bind(TraffLocation.Directionality.class, new NullEnumTransform(TraffLocation.Directionality.class));
		XML_MATCHER.bind(TraffLocation.Fuzziness.class, new NullEnumTransform(TraffLocation.Fuzziness.class));
		XML_MATCHER.bind(TraffLocation.Ramps.class, new NullEnumTransform(TraffLocation.Ramps.class));
		XML_MATCHER.bind(TraffLocation.RoadClass.class, new NullEnumTransform(TraffLocation.RoadClass.class));
	}
	
	// use a custom strategy to avoid name collisions (TraFF uses `class` as an attribute name)
	static final Strategy XML_STRATEGY = new TreeStrategy("simpleXmlClass", "simpleXmlLength");
	public static final Serializer XML_SERIALIZER = new Persister(XML_STRATEGY, XML_MATCHER);

	/**
	 * Converts a collection of messages to an XML feed.
	 * 
	 * @param messages The messages to include in the feed
	 * @return The XML stream for the feed
	 * @throws Exception if any errors occur during serialization
	 */
	@Deprecated
	public static String createFeed(List<TraffMessage> messages) throws Exception {
		return new TraffFeed(messages).toXml();
	}

	@Deprecated
	static String getIndentString(int indent) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < indent; i++)
			builder.append(' ');
		return builder.toString();
	}

	static DateFormat getIsoDateFormat() {
		DateFormat res;
		try {
			res = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX", Locale.ROOT);
		} catch (IllegalArgumentException e) {
			/*
			 * Older Android versions do not support X as a placeholder for the date format.
			 * On those versions, fall back to Z. Side effect is that the parser will insist on a time
			 * zone format of +hhmm, throwing an exception upon encountering +hh, +hh:mm or a literal Z.
			 */
			res = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ROOT);
		}
		return res;
	}
}
