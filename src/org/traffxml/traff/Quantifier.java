/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff;

/**
 * The abstract base class for quantifiers, which can be attached to events and supplementary information items.
 */
public abstract class Quantifier {
	/** The quantifier type for use in XML. Descendants must override this and provide their own value. */
	public final String type = "q_invalid";

	/**
	 * Returns the quantifier formatted as an XML attribute.
	 */
	@Deprecated
	public abstract String toAttribute();
}
