package org.traffxml.traff.util;

import org.simpleframework.xml.transform.Transform;

/**
 * Represents a transform that is used to transform enumerations to strings and back again, falling back to
 * null when deserializing illegal enum values.
 * 
 * This is used when enumerations are used in comma separated arrays. This may be created multiple times for
 * different types.
 */
public class NullEnumTransform implements Transform<Enum> {

	/**
	 * The specific enumeration that this instance transforms.
	 */
	private final Class type;

	/**
	 * Constructor for the <code>NullEnumTransform</code> object. This
	 * is used to create enumerations from strings and convert them
	 * back again. This allows enumerations to be used in arrays.
	 * 
	 * @param type the enumeration type to be transformed
	 */
	public NullEnumTransform(Class type) {
		this.type = type;
	}

	/**
	 * Converts the string value given to an appropriate representation.
	 * 
	 * This is used when an object is being deserialized from the XML document and the value for
	 * the string representation is required.
	 * 
	 * @param value the string representation of the value
	 * 
	 * @return an appropriate instanced to be used, or null
	 */
	public Enum read(String value) throws Exception {
		try {
			return Enum.valueOf(type, value);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	/**
	 * Converts the provided value into an XML usable format.
	 * 
	 * This is used in the serialization process when there is a need to convert a field value in to a
	 * string so that that value can be written as a valid XML entity.
	 * 
	 * @param value the value to be converted to a string
	 * 
	 * @return the string representation of the given value
	 */
	public String write(Enum value) throws Exception {
		return value.name();
	}
}
