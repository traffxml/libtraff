/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

/**
 * Encapsulates the location data for a {@link TraffMessage}.
 * 
 * <p>As of version 0.8, a location cannot span multiple roads, i.e. at least one of the following must be true:
 * The road number remains unchanged throughout the location, the road name remains unchanged throughout the
 * location, or the stretch of road to which the location refers must not connect to any other road of the
 * same or a higher category.
 * 
 * <p>Some sections are shared by multiple roads and bear multiple road numbers. They are considered as
 * belonging to either road and may be referenced by either number. A location may at the same time span a
 * shared section and no more than one of the roads it belongs to, and reference them with the respective
 * road number.
 * 
 * <p>New locations are generated using a {@link Builder}.
 */
@Root(strict=false, name="location")
@Order(elements={"from", "to", "at", "via", "not_via"},
attributes={"country", "destination", "direction", "directionality", "fuzziness", "origin", "ramps", "road_class", "road_is_urban", "road_ref", "road_name", "territory", "town"})
public class TraffLocation {
	public static enum Directionality {
		ONE_DIRECTION,
		BOTH_DIRECTIONS
	}

	public static enum Fuzziness {
		NONE,
		LOW_RES,
		MEDIUM_RES,
		END_UNKNOWN,
		START_UNKNOWN,
		EXTENT_UNKNOWN
	}

	public static enum Ramps {
		NONE,
		ALL,
		ENTRY,
		EXIT
	}

	public static enum RoadClass {
		INVALID,
		MOTORWAY,
		TRUNK,
		PRIMARY,
		SECONDARY,
		TERTIARY,
		OTHER
	}

	/**
	 * A simple matcher.
	 * 
	 * <p>This matcher returns true if, and only if, both locations have the exact same set of points
	 * set to null, and all other points have the exact same coordinates if they have the same role.
	 * Other attributes of the locations, or any of their points, are ignored.
	 */
	public static final Matcher DEFAULT_MATCHER = new Matcher() {
		@Override
		public boolean match(TraffLocation l1, TraffLocation l2) {
			if (!matchPoint(l1.from, l2.from))
				return false;
			if (!matchPoint(l1.at, l2.at))
				return false;
			if (!matchPoint(l1.notVia, l2.notVia))
				return false;
			if (!matchPoint(l1.via, l2.via))
				return false;
			if (!matchPoint(l1.to, l2.to))
				return false;
			return true;
		}

		private boolean matchPoint(Point p1, Point p2) {
			if ((p1 == null) && (p2 == null))
				return true;
			if ((p1 == null) || (p2 == null))
				return false;
			return p1.coordinates.equals(p2.coordinates);
		}
	};

	/**
	 * The ISO country code for the location.
	 */
	@Attribute(required=false)
	public final String country;

	/**
	 * A destination, preferably the one given on road signs, which a driver would encounter after (or at) the {@code to} location.
	 */
	@Attribute(required=false)
	public final String destination;

	/**
	 * A compass direction indicating the direction of travel to which this message applies.
	 * 
	 * <p>This member is only relevant for single-point locations (with all {@link Point} members other than
	 * {@code at} set to null). If multiple points are set (which is the preferred way of indicating
	 * directions), they should be used to infer direction and this member ignored.
	 * 
	 * <p>Optional for monodirectional locations, forbidden for bidirectional locations.
	 * 
	 * <p>Discouraged for ring roads (including partial ring roads) or for sections which significantly deviate
	 * from the principal direction of the main road.
	 * 
	 * <p>Permissible values include “N” or “SE”.
	 */
	@Attribute(required=false)
	public final String direction;

	/**
	 * Whether the message refers to one or both directions of travel.
	 */
	@Attribute
	public final Directionality directionality;

	/**
	 * Precision of the point members.
	 * 
	 * <p>If null, point members should be treated as exact coordinates, i.e. referring to the nearest point on
	 * the road (or carriageway in the given direction).
	 */
	@Attribute(required=false)
	public final Fuzziness fuzziness;

	/**
	 * A destination, preferably the one given on road signs, which a driver would encounter before (or at) the {@code from} location.
	 */
	@Attribute(required=false)
	public final String origin;

	/**
	 * Whether the location affects the main carriageway or the ramps.
	 * 
	 * <p>If set to anything other than {@code NONE}, this indicates that the event does not refer to the main
	 * carriageway of the road but to the ramps which connect to the affected section.
	 * 
	 * <p>This is mainly intended for compatibility with TMC or other data sources where junctions with all
	 * their ramps are represented by a single point. Other sources should instead use coordinate pairs
	 * located directly on the ramps, and set this member to {@code NONE}.
	 */
	@Attribute(required=false)
	public final Ramps ramps;

	/**
	 * The importance of the affected road within the road network.
	 * 
	 * <p>Classification roughly corresponds to OpenStreetMap.
	 */
	@Attribute(required=false, name="road_class")
	public final RoadClass roadClass;

	/**
	 * Whether the affected stretch of road is in a built-up area.
	 */
	@Attribute(required=false, name="road_is_urban")
	public final Boolean roadIsUrban;

	/**
	 * The name of the stretch of road affected.
	 * 
	 * <p>Examples: “Savanorių prospektas”, “Viale Certosa”, “Wasserburger Landstraße”
	 * 
	 * <p>Do not use this attribute if the road name is not consistent throughout the entire location.
	 */
	@Attribute(required=false, name="road_name")
	public final String roadName;

	/**
	 * The number of the stretch of road affected in the road network.
	 * 
	 * <p>Example: SP526
	 * 
	 * <p>Do not use this attribute if the road number is not consistent throughout the entire location.
	 */
	@Attribute(required=false, name="road_ref")
	public final String roadRef;

	/**
	 * A territorial entity within a country, where applicable and needed for disambiguation.
	 * 
	 * <p>Usually denoted by its official shorthand where available and widely used, else the local name.
	 * 
	 * <p>Can be omitted if {@link #town} is specified and the combination of {@link #country} and {@link #town}
	 * is unambiguous.
	 */
	@Attribute(required=false)
	public final String territory;

	/**
	 * A town or city, designated by its local name, where applicable and needed for disambiguation.
	 * 
	 * <p>Generally needed for local street names.
	 */
	@Attribute(required=false)
	public final String town;

	/**
	 * The starting point of the location.
	 * 
	 * <p>The starting point is the point at which the driver would first encounter the condition reported. If
	 * {@code at} is specified, this element merely serves to indicate the direction, and the condition is
	 * limited to the point described by {@code at}.
	 * 
	 * <p>Required for linear features and directional point locations on ring roads. For directional point
	 * locations on non-ring roads, either {@code from} or {@code to} must be specified along with {@code at}.
	 */
	@Element(required=false)
	public final Point from;

	/**
	 * The coordinates of the condition reported.
	 * 
	 * <p>If {@code from} and/or {@code to} are specified, they merely serve to indicate the direction, and the
	 * condition is limited to the point described by {@code at}.
	 * 
	 * Required for point locations on ring roads. For directional point locations on non-ring roads, either
	 * {@code from} or {@code to} must be specified along with {@code at}. For directional point locations on
	 * ring roads, all of {@code from}, {@code at} and {@code to} must be specified and no two points may
	 * coincide.
	 */
	@Element(required=false)
	public final Point at;

	/**
	 * A location not on the affected stretch, indicating the direction of a ring road to which the message applies.
	 * 
	 * <p>This member refers to a point that a driver might pass before entering the affected stretch or after leaving it,
	 * but not in between.
	 * 
	 * <p>Either {@code via} or {@code not_via} must be present for linear locations on ring roads. Both
	 * {@code from} and {@code to} must be specified along with {@code via}. Do not use for point locations.
	 */
	@Element(required=false, name="not_via")
	public final Point notVia;

	/**
	 * A location on the affected stretch, indicating the direction of a ring road to which the message applies.
	 * 
	 * <p>This member refers to a point that a driver would pass on the way between {@code from} and {@code to}.
	 * 
	 * <p>Either {@code via} or {@code not_via} must be present for linear locations on ring roads. Both
	 * {@code from} and {@code to} must be specified along with {@code via}. Do not use for point locations.
	 */
	@Element(required=false)
	public final Point via;

	/**
	 * The end point of the location.
	 * 
	 * <p>The end point is the point at which the driver would encounter the end of the condition reported. If
	 * {@code at} is specified, this element merely serves to indicate the direction, and the condition is
	 * limited to the point described by {@code at}.
	 * 
	 * <p>Required for linear features and directional point locations on ring roads. For directional point
	 * locations on non-ring roads, either {@code from} or {@code to} must be specified along with {@code at}.
	 */
	@Element(required=false)
	public final Point to;
/*
	private TraffLocation(Point at, Point from, Point to, Point via, Point notVia, String country,
			String origin, String destination, String direction, Directionality directionality,
			Fuzziness fuzziness, Ramps ramps, RoadClass roadClass, Boolean roadIsUrban, String roadName,
			String roadRef, String territory, String town) {
		this.at = at;
		this.from = from;
		this.to = to;
		this.via = via;
		this.notVia = notVia;
		this.country = country;
		this.destination = destination;
		this.direction = direction;
		this.directionality = directionality;
		this.fuzziness = fuzziness;
		this.origin = origin;
		this.ramps = ramps;
		this.roadClass = roadClass;
		this.roadIsUrban = roadIsUrban;
		this.roadName = roadName;
		this.roadRef = roadRef;
		this.territory = territory;
		this.town = town;
	}
*/
	/**
	 * Constructor for XML deserialization.
	 * 
	 * @param at
	 * @param from
	 * @param to
	 * @param via
	 * @param notVia
	 * @param country
	 * @param origin
	 * @param destination
	 * @param direction
	 * @param directionality
	 * @param fuzziness
	 * @param ramps
	 * @param roadClass
	 * @param roadIsUrban
	 * @param roadName
	 * @param roadRef
	 * @param territory
	 * @param town
	 */
	public TraffLocation(@Element(name="at") Point at,
			@Element(name="from") Point from,
			@Element(name="to") Point to,
			@Element(name="via") Point via,
			@Element(name="not_via") Point notVia,
			@Attribute(name="country") String country,
			@Attribute(name="origin") String origin,
			@Attribute(name="destination") String destination,
			@Attribute(name="direction") String direction,
			@Attribute(name="directionality") Directionality directionality,
			@Attribute(name="fuzziness") Fuzziness fuzziness,
			@Attribute(name="ramps") Ramps ramps,
			@Attribute(name="road_class") RoadClass roadClass,
			@Attribute(name="road_is_urban") Boolean roadIsUrban,
			@Attribute(name="road_name") String roadName,
			@Attribute(name="road_ref") String roadRef,
			@Attribute(name="territory") String territory,
			@Attribute(name="town") String town) {
		this.at = at;
		this.from = from;
		this.to = to;
		this.via = via;
		this.notVia = notVia;
		this.country = country;
		this.destination = destination;
		this.direction = direction;
		if (directionality != null)
			this.directionality = directionality;
		else
			this.directionality = Directionality.BOTH_DIRECTIONS;
		this.fuzziness = fuzziness;
		this.origin = origin;
		this.ramps = ramps;
		this.roadClass = roadClass;
		this.roadIsUrban = roadIsUrban;
		this.roadName = roadName;
		this.roadRef = roadRef;
		this.territory = territory;
		this.town = town;
	}

	/**
	 * Returns the bounding box for the location.
	 * 
	 * @return A BoundingBox which encloses all reference points
	 */
	public BoundingBox getBoundingBox() {
		Point[] points = {from, at, via, to, notVia};
		BoundingBox bbox = null;
		for (Point point : points) {
			if (point == null)
				continue;
			if (bbox == null)
				bbox = new BoundingBox(point.coordinates);
			else
				bbox = bbox.extend(point.coordinates);
		}
		return bbox;
	}

	/**
	 * Returns the points of the location as a map.
	 * 
	 * <p>This is intended for use cases which need to iterate over all the points of a location.
	 * 
	 * @return A map of the non-null points
	 */
	public Map<Point.Role, Point> getPointMap() {
		Map<Point.Role, Point> res = new HashMap<Point.Role, Point>();
		if (from != null)
			res.put(Point.Role.FROM, from);
		if (at != null)
			res.put(Point.Role.AT, at);
		if (to != null)
			res.put(Point.Role.TO, to);
		if (via != null)
			res.put(Point.Role.VIA, via);
		if (notVia != null)
			res.put(Point.Role.NOT_VIA, notVia);
		return res;
	}

	/**
	 * Returns the location in TraFF XML format.
	 * 
	 * <p>Calling this method is equivalent to calling {@link #toXml(int)} with a zero argument. That is, the
	 * root element will not be indented. 
	 */
	@Deprecated
	public String toXml() {
		return toXml(0);
	}

	/**
	 * Returns the location in TraFF XML format.
	 * 
	 * @param indent Indentation for the root element
	 */
	@Deprecated
	public String toXml(int indent) {
		String indentStr = Traff.getIndentString(indent);
		StringBuilder builder = new StringBuilder();

		/* opening tag with attributes */
		builder.append(indentStr).append("<location");
		if (this.country != null)
			builder.append(String.format(" country=\"%s\"", this.country));
		if (this.destination != null)
			builder.append(String.format(" destination=\"%s\"", this.destination));
		if (this.direction != null)
			builder.append(String.format(" direction=\"%s\"", this.direction));
		if (this.directionality != null)
			builder.append(String.format(" directionality=\"%s\"", this.directionality.name()));
		if (this.fuzziness != null)
			builder.append(String.format(" fuzziness=\"%s\"", this.fuzziness.name()));
		if (this.origin != null)
			builder.append(String.format(" origin=\"%s\"", this.origin));
		if (this.ramps != null)
			builder.append(String.format(" ramps=\"%s\"", this.ramps.name()));
		if (this.roadClass != null)
			builder.append(String.format(" road_class=\"%s\"", this.roadClass.name()));
		if (this.roadIsUrban != null)
			builder.append(String.format(" road_is_urban=\"%b\"", this.roadIsUrban));
		if (this.roadRef != null)
			builder.append(String.format(" road_ref=\"%s\"", this.roadRef));
		if (this.roadName != null)
			builder.append(String.format(" road_name=\"%s\"", this.roadName));
		if (this.territory != null)
			builder.append(String.format(" territory=\"%s\"", this.territory));
		if (this.town != null)
			builder.append(String.format(" town=\"%s\"", this.town));
		builder.append(">\n");

		/* reference points */
		if (this.from != null)
			builder.append(this.from.toXml(indent + Traff.INDENT, "from"));
		if (this.to != null)
			builder.append(this.to.toXml(indent + Traff.INDENT, "to"));
		if (this.at != null)
			builder.append(this.at.toXml(indent + Traff.INDENT, "at"));
		if (this.via != null)
			builder.append(this.via.toXml(indent + Traff.INDENT, "via"));
		if (this.notVia != null)
			builder.append(this.notVia.toXml(indent + Traff.INDENT, "not_via"));

		/* closing tag */
		builder.append(indentStr).append("</location>\n");
		return builder.toString();
	}

	/**
	 * Encapsulates one of the reference points for a {@link TraffLocation}.
	 */
	@Root(strict=false)
	@Order(attributes={"distance", "junction_name", "junction_ref"})
	public static class Point {
		public static enum Role {
			FROM,
			AT,
			TO,
			VIA,
			NOT_VIA
		}

		/** The coordinates of this point. */
		public final LatLon coordinates;

		/**
		 * Distance along the road, in km.
		 * 
		 * <p>Distances are given according to the official kilometric points on the road. These are not
		 * necessarily contiguous: if roads have been rerouted or renumbered, ranges may be missing or
		 * doubled, or the direction may change. For this reason, this member should not be used to calculate
		 * distances.
		 */
		@Attribute(required=false)
		public final Float distance;

		/**
		 * The name of a motorway junction, if this point refers to a junction.
		 * 
		 * <p>Leave this member set to null if the location does not actually refer to a junction. Do not use it
		 * to indicate the nearest junction.
		 */
		@Attribute(required=false, name="junction_name")
		public final String junctionName;

		/**
		 * The reference number of a motorway junction, if this point refers to a junction with such a number.
		 * 
		 * <p>Leave this member set to null if the location does not actually refer to a junction. Do not use it
		 * to indicate the nearest junction.
		 * 
		 * <p>On roads which do not number their junctions, leave this member set to null.
		 */
		@Attribute(required=false, name="junction_ref")
		public final String junctionRef;

		/** @deprecated Use a {@link Point.Builder} instead. */
		@Deprecated
		public Point(float lat, float lon, String junctionName, String junctionRef) {
			this(new LatLon(lat, lon), junctionName, junctionRef);
		}

		/** @deprecated Use a {@link Point.Builder} instead. */
		@Deprecated
		public Point(LatLon coordinates, String junctionName, String junctionRef) {
			this(coordinates, null, junctionName, junctionRef);
		}

		/**
		 * Constructor for XML deserialization.
		 * 
		 * @param coordinates
		 * @param distance
		 * @param junctionName
		 * @param junctionRef
		 * @throws ParseException if {@code coordinates} does not contain a valid coordinate pair
		 */
		public Point(@Text String coordinates,
				@Attribute(name="distance") Float distance,
				@Attribute(name="junction_name") String junctionName,
				@Attribute(name="junction_ref") String junctionRef) throws ParseException {
			this(new LatLon(coordinates), distance, junctionName, junctionRef);
		}

		@Text
		public String getCoordinatesAsText() {
			return String.format("%f %f", coordinates.lat, coordinates.lon);
		}

		private Point(LatLon coordinates, Float distance, String junctionName, String junctionRef) {
			super();
			this.distance = distance;
			this.coordinates = coordinates;
			this.junctionName = junctionName;
			this.junctionRef = junctionRef;
		}

		private String toXml(int indent, String name) {
			StringBuilder indentBuilder = new StringBuilder();
			for (int i = 0; i < indent; i++)
				indentBuilder.append(' ');
			String indentStr = indentBuilder.toString();
			StringBuilder builder = new StringBuilder();

			builder.append(indentStr).append(String.format("<%s", name));
			if (this.distance != null)
				builder.append(String.format(" distance=\"%.3f\"", this.distance));
			if (this.junctionName != null)
				builder.append(String.format(" junction_name=\"%s\"", this.junctionName));
			if (this.junctionRef != null)
				builder.append(String.format(" junction_ref=\"%s\"", this.junctionRef));
			builder.append(">");
			builder.append(getCoordinatesAsText());
			builder.append(String.format("</%s>\n", name));
			return builder.toString();
		}

		public static class Builder {
			LatLon coordinates = null;
			Float distance = null;
			String junctionName = null;
			String junctionRef = null;

			public Builder() {
				super();
			}

			public Builder(Point point) {
				super();
				this.coordinates = point.coordinates;
				this.distance = point.distance;
				this.junctionName = point.junctionName;
				this.junctionRef = point.junctionRef;
			}

			public void setCoordinates(LatLon coordinates) {
				this.coordinates = coordinates;
			}

			public void setDistance(Float distance) {
				this.distance = distance;
			}

			public void setJunctionName(String junctionName) {
				this.junctionName = junctionName;
			}

			public void setJunctionRef(String junctionRef) {
				this.junctionRef = junctionRef;
			}

			public Point build() {
				if (coordinates == null)
					throw new IllegalStateException("coordinates must be set");
				return new Point(coordinates, distance, junctionName, junctionRef);
			}
		}
	}

	/**
	 * Used to build a new {@link TraffLocation}.
	 */
	public static class Builder {
		String country = null;
		String destination = null;
		String direction = null;
		Directionality directionality = null;
		Fuzziness fuzziness = null;
		String origin = null;
		Ramps ramps = null;
		RoadClass roadClass = null;
		Boolean roadIsUrban = null;
		String roadName = null;
		String roadRef = null;
		String territory = null;
		String town = null;
		Point from = null;
		Point at = null;
		Point notVia = null;
		Point via = null;
		Point to = null;

		public Builder() {
			super();
		}

		/**
		 * Initializes a new builder with the data of an existing {@link TraffLocation}.
		 */
		public Builder(TraffLocation location) {
			super();
			if (location == null)
				return;
			at = location.at;
			from = location.from;
			to = location.to;
			via = location.via;
			notVia = location.notVia;
			country = location.country;
			destination = location.destination;
			direction = location.direction;
			directionality = location.directionality;
			fuzziness = location.fuzziness;
			origin = location.origin;
			ramps = location.ramps;
			roadClass = location.roadClass;
			roadIsUrban = location.roadIsUrban;
			roadName = location.roadName;
			roadRef = location.roadRef;
			territory = location.territory;
			town = location.town;
		}

		/**
		 * Builds a new {@link TraffMessage}.
		 * 
		 * <p>The builder performs a basic consistency check on the message. Bidirectional locations must not
		 * specify a destination or direction (if directionality is null, the location is bidirectional).
		 * Either {@code at} point or a pair of {@code from} and {@code to} points must be set. If {@code at}
		 * is set, neither of {@code via} or {@code not_via} may be set. If any of these criteria is not
		 * satisfied, an {@link IllegalStateException} is thrown.
		 */
		public TraffLocation build() {
			if (!Directionality.ONE_DIRECTION.equals(directionality)) {
				if (direction != null)
					throw new IllegalStateException("direction is not permitted for a bidirectional location");
			}
			if (at == null) {
				if (from == null)
					throw new IllegalStateException("from and at cannot be null at the same time");
				if (to == null)
					throw new IllegalStateException("at and to cannot be null at the same time");
			} else {
				if (via != null)
					throw new IllegalStateException("at and via cannot be specified together");
				if (notVia != null)
					throw new IllegalStateException("at and not_via cannot be specified together");
			}
			return new TraffLocation(at, from, to, via, notVia, country, origin, destination, direction,
					directionality, fuzziness, ramps, roadClass, roadIsUrban, roadName, roadRef, territory,
					town);
		}

		/**
		 * Inverts the direction of the location, to the extent possible.
		 * 
		 * <p>Inverting the direction swaps the {@code from} and {@code to} points, as well as the
		 * {@code origin} and {@code destination} names. Note that other fields, notably {@code direction},
		 * are not updated and may be inconsistent after that.
		 */
		public void invert() {
			Point tmp = to;
			to = from;
			from = tmp;
			String tmpStr = origin;
			origin = destination;
			destination = tmpStr;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public void setDestination(String destination) {
			this.destination = destination;
		}

		public void setDirection(String direction) {
			this.direction = direction;
		}

		public void setDirectionality(Directionality directionality) {
			this.directionality = directionality;
		}

		public void setFuzziness(Fuzziness fuzziness) {
			this.fuzziness = fuzziness;
		}

		public void setOrigin(String origin) {
			this.origin = origin;
		}

		public void setRamps(Ramps ramps) {
			this.ramps = ramps;
		}

		public void setRoadClass(RoadClass roadClass) {
			this.roadClass = roadClass;
		}

		public void setRoadIsUrban(Boolean roadIsUrban) {
			this.roadIsUrban = roadIsUrban;
		}

		public void setRoadName(String roadName) {
			this.roadName = roadName;
		}

		public void setRoadRef(String roadRef) {
			this.roadRef = roadRef;
		}

		public void setTerritory(String territory) {
			this.territory = territory;
		}

		public void setTown(String town) {
			this.town = town;
		}

		public void setPoint(Point.Role role, Point point) {
			switch(role) {
			case FROM:
				setFrom(point);
				break;
			case AT:
				setAt(point);
				break;
			case NOT_VIA:
				setNotVia(point);
				break;
			case VIA:
				setVia(point);
				break;
			case TO:
				setTo(point);
				break;
			}
		}

		public void setFrom(Point from) {
			this.from = from;
		}

		public void setAt(Point at) {
			this.at = at;
		}

		public void setNotVia(Point notVia) {
			this.notVia = notVia;
		}

		public void setVia(Point via) {
			this.via = via;
		}

		public void setTo(Point to) {
			this.to = to;
		}
	}

	/**
	 * A matching function, which determines if two {@code TraffLocation} instances refer to the same location.
	 * 
	 * <p>Matching two locations is not an exact science, as there are many ways to represent one and
	 * the same location: Minute variations in the coordinates of the constituent points usually
	 * will not result in a different location. The choice of auxiliary points is almost unlimited.
	 * Name and road class attributes may differ. A definite decision often requires a map, which
	 * may not be available in all situations. Different use cases may have different requirements.
	 * 
	 * <p>This can be accommodated with different {@code Matcher} implementations for each use case.
	 * 
	 * <p>A default matcher is available as {@link #DEFAULT_MATCHER}.
	 */
	@FunctionalInterface
	public static interface Matcher {
		public abstract boolean match(TraffLocation l1, TraffLocation l2);
	}
}
