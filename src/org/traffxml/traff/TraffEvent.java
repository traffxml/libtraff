/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;

/**
 * Encapsulates an event.
 * 
 * <p>An event refers to the cause of a traffic disruption or its impact on traffic, in machine-parseable form.
 * 
 * <p>New locations are generated using a {@link Builder}.
 */
@Root(strict=false, name="event")
@Order(attributes={"class", "type", "length", "probability", "speed"})
public class TraffEvent {
	public static enum Class {
		INVALID,
		/** @since 0.8 */
		ACTIVITY,
		/** @deprecated Reserved for future use. */
		@Deprecated
		AUDIO_BROADCAST,
		/** @since 0.8 */
		AUTHORITY,
		/** @since 0.8 */
		CARPOOL,
		CONGESTION,
		/** @since 0.8 */
		CONSTRUCTION,
		DELAY,
		/** @since 0.8 */
		ENVIRONMENT,
		/** @since 0.8 */
		EQUIPMENT_STATUS,
		/** @since 0.8 */
		HAZARD,
		/** @since 0.8 */
		INCIDENT,
		/** @deprecated Reserved for future use. */
		@Deprecated
		PARKING,
		RESTRICTION,
		/** @since 0.8 */
		SECURITY,
		/** @deprecated Reserved for future use. */
		@Deprecated
		SERVICE,
		/** @since 0.8 */
		TRANSPORT,
		/** @deprecated Reserved for future use. */
		@Deprecated
		TRAVEL_TIME,
		/** @since 0.8 */
		WEATHER
	}

	public static enum Type {
		INVALID,
		/** @since 0.8 */
		ACTIVITY_ATHLETICS_MEETING,
		/** @since 0.8 */
		ACTIVITY_AUTOMOBILE_RACE,
		/** @since 0.8 */
		ACTIVITY_BALL_GAME,
		/** @since 0.8 */
		ACTIVITY_BASEBALL_GAME,
		/** @since 0.8 */
		ACTIVITY_BASKETBALL_GAME,
		/** @since 0.8 */
		ACTIVITY_BOAT_RACE,
		/** @since 0.8 */
		ACTIVITY_BOXING_TOURNAMENT,
		/** @since 0.8 */
		ACTIVITY_BULL_FIGHT,
		/** @since 0.8 */
		ACTIVITY_CEREMONIAL_EVENT,
		/** @since 0.8 */
		ACTIVITY_CONCERT,
		/** @since 0.8 */
		ACTIVITY_CRICKET_MATCH,
		/** @since 0.8 */
		ACTIVITY_CROWD,
		/** @since 0.8 */
		ACTIVITY_CYCLE_RACE,
		/** @since 0.8 */
		ACTIVITY_DEMONSTRATION,
		/** @since 0.8 */
		ACTIVITY_EVENT,
		/** @since 0.8 */
		ACTIVITY_EXHIBITION,
		/** @since 0.8 */
		ACTIVITY_FAIR,
		/** @since 0.8 */
		ACTIVITY_FESTIVAL,
		/** @since 0.8 */
		ACTIVITY_FILM_TV_PRODUCTION,
		/** @since 0.8 */
		ACTIVITY_FUNFAIR,
		/** @since 0.8 */
		ACTIVITY_GOLF_TOURNAMENT,
		/** @since 0.8 */
		ACTIVITY_HOCKEY_GAME,
		/** @since 0.8 */
		ACTIVITY_INTERNATIONAL_SPORTS_EVENT,
		/** @since 0.8 */
		ACTIVITY_MAJOR_EVENT,
		/** @since 0.8 */
		ACTIVITY_MARATHON,
		/** @since 0.8 */
		ACTIVITY_MARCH,
		/** @since 0.8 */
		ACTIVITY_MARKET,
		/** @since 0.8 */
		ACTIVITY_MATCH,
		/** @since 0.8 */
		ACTIVITY_PARADE,
		/** @since 0.8 */
		ACTIVITY_PROCESSION,
		/** @since 0.8 */
		ACTIVITY_PROTEST,
		/** @since 0.8 */
		ACTIVITY_PUBLIC_DISTURBANCE,
		/** @since 0.8 */
		ACTIVITY_RACE_MEETING,
		/** @since 0.8 */
		ACTIVITY_RUGBY_MATCH,
		/** @since 0.8 */
		ACTIVITY_SEVERAL_EVENTS,
		/** @since 0.8 */
		ACTIVITY_SHOW,
		/** @since 0.8 */
		ACTIVITY_SHOW_JUMPING,
		/** @since 0.8 */
		ACTIVITY_SOCCER_MATCH,
		/** @since 0.8 */
		ACTIVITY_SPORTS_EVENT,
		/** @since 0.8 */
		ACTIVITY_STATE_OCCASION,
		/** @since 0.8 */
		ACTIVITY_STRIKE,
		/** @since 0.8 */
		ACTIVITY_TENNIS_TOURNAMENT,
		/** @since 0.8 */
		ACTIVITY_TOURNAMENT,
		/** @since 0.8 */
		ACTIVITY_TRADE_FAIR,
		/** @since 0.8 */
		ACTIVITY_WATER_SPORTS_MEETING,
		/** @since 0.8 */
		ACTIVITY_WINTER_SPORTS_MEETING,
		/** @since 0.8 */
		AUTHORITY_CHECKPOINT,
		/** @since 0.8 */
		AUTHORITY_INVESTIGATION,
		/** @since 0.8 */
		AUTHORITY_SPEED_CHECKS,
		/** @since 0.8 */
		AUTHORITY_VIP_PASSING,
		/** @since 0.8 */
		AUTHORITY_WINTER_EQUIPMENT_CHECKS,
		/** @since 0.8 */
		CARPOOL_LANE_OPERATIONAL,
		CONGESTION_CLEARED,
		CONGESTION_FORECAST_WITHDRAWN,
		CONGESTION_HEAVY_TRAFFIC,
		CONGESTION_LONG_QUEUE,
		CONGESTION_NONE,
		CONGESTION_NORMAL_TRAFFIC,
		CONGESTION_QUEUE,
		CONGESTION_QUEUE_LIKELY,
		CONGESTION_SLOW_TRAFFIC,
		CONGESTION_STATIONARY_TRAFFIC,
		CONGESTION_STATIONARY_TRAFFIC_LIKELY,
		CONGESTION_TRAFFIC_BUILDING_UP,
		CONGESTION_TRAFFIC_CONGESTION,
		CONGESTION_TRAFFIC_EASING,
		CONGESTION_TRAFFIC_FLOWING_FREELY,
		CONGESTION_TRAFFIC_HEAVIER_THAN_NORMAL,
		CONGESTION_TRAFFIC_LIGHTER_THAN_NORMAL,
		CONGESTION_TRAFFIC_MUCH_HEAVIER_THAN_NORMAL,
		CONGESTION_TRAFFIC_PROBLEM,
		/** @since 0.8 */
		CONSTRUCTION_BLASTING,
		/** @since 0.8 */
		CONSTRUCTION_BRIDGE_MAINTENANCE,
		/** @since 0.8 */
		CONSTRUCTION_BURIED_CABLES,
		/** @since 0.8 */
		CONSTRUCTION_BURIED_SERVICES,
		/** @since 0.8 */
		CONSTRUCTION_CONSTRUCTION_WORK,
		/** @since 0.8 */
		CONSTRUCTION_DEMOLITION_WORK,
		/** @since 0.8 */
		CONSTRUCTION_GAS_MAINS,
		/** @since 0.8 */
		CONSTRUCTION_LONGTERM_ROADWORKS,
		/** @since 0.8 */
		CONSTRUCTION_MAINTENANCE,
		/** @since 0.8 */
		CONSTRUCTION_MAJOR_ROADWORKS,
		/** @since 0.8 */
		CONSTRUCTION_NEW_ROADWORKS_LAYOUT,
		/** @since 0.8 */
		CONSTRUCTION_REPAIR,
		/** @since 0.8 */
		CONSTRUCTION_RESURFACING_WORK,
		/** @since 0.8 */
		CONSTRUCTION_ROADWORK_CLEARANCE,
		/** @since 0.8 */
		CONSTRUCTION_ROADWORKS,
		/** @since 0.8 */
		CONSTRUCTION_TEMP_TRAFFIC_LIGHT,
		/** @since 0.8 */
		CONSTRUCTION_WATER_MAINS,
		DELAY_CLEARANCE,
		DELAY_DELAY,
		DELAY_DELAY_POSSIBLE,
		DELAY_FORECAST_WITHDRAWN,
		DELAY_LONG_DELAY,
		DELAY_SEVERAL_HOURS,
		DELAY_UNCERTAIN_DURATION,
		DELAY_VERY_LONG_DELAY,
		/** @since 0.8 */
		ENVIRONMENT_EXHAUST_POLLUTION,
		/** @since 0.8 */
		ENVIRONMENT_SMOG,
		/** @since 0.8 */
		EQUIPMENT_STATUS_AUTOMATIC_TOLL_SYSTEM_NOT_WORKING,
		/** @since 0.8 */
		EQUIPMENT_STATUS_EMERGENCY_PHONES_NOT_WORKING,
		/** @since 0.8 */
		EQUIPMENT_STATUS_LANE_CONTROL_SIGNS_NOT_WORKING,
		/** @since 0.8 */
		EQUIPMENT_STATUS_LANE_CONTROL_SIGNS_WORKING_INCORRECTLY,
		/** @deprecated Use {@link #EQUIPMENT_STATUS_LEVEL_CROSSING_NOT_WORKING} instead. */
		@Deprecated
		EQUIPMENT_STATUS_LEVEL_CROSSING_FAILURE,
		/** @since 0.8 */
		EQUIPMENT_STATUS_LEVEL_CROSSING_NOT_WORKING,
		/** @since 0.8 */
		EQUIPMENT_STATUS_RAMP_SIGNALS_NOT_WORKING,
		/** @since 0.8 */
		EQUIPMENT_STATUS_RAMP_SIGNALS_WORKING_INCORRECTLY,
		/** @since 0.8 */
		EQUIPMENT_STATUS_TEMPORARY_TRAFFIC_LIGHTS_NOT_WORKING,
		/** @since 0.8 */
		EQUIPMENT_STATUS_TEMPORARY_TRAFFIC_LIGHTS_WORKING_INCORRECTLY,
		/** @since 0.8 */
		EQUIPMENT_STATUS_TRAFFIC_LIGHTS_NOT_WORKING,
		/** @since 0.8 */
		EQUIPMENT_STATUS_TRAFFIC_LIGHTS_WORKING_INCORRECTLY,
		/** @since 0.8 */
		HAZARD_ABNORMAL_LOAD,
		/** @since 0.8 */
		HAZARD_AIR_CRASH,
		/** @since 0.8 */
		HAZARD_ALMOST_IMPASSABLE,
		/**
		 * @deprecated Use {@link #HAZARD_ALMOST_IMPASSABLE} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		@Deprecated
		HAZARD_ALMOST_IMPASSABLE_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_ANIMAL_HERDS_ON_ROAD,
		/** @since 0.8 */
		HAZARD_ANIMALS_ON_ROAD,
		/** @since 0.8 */
		HAZARD_AVALANCHE,
		/** @since 0.8 */
		HAZARD_BAD_ROAD,
		/** @since 0.8 */
		HAZARD_BLACK_ICE,
		/**
		 * @deprecated Use {@link #HAZARD_BLACK_ICE} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_BLACK_ICE_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_BLACK_ICE_LIKELY,
		/**
		 * @deprecated Use {@link #HAZARD_BLACK_ICE_LIKELY} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_BLACK_ICE_LIKELY_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_BLOCKED_BY_SNOW,
		/**
		 * @deprecated Use {@link #HAZARD_BLOCKED_BY_SNOW} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_BLOCKED_BY_SNOW_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_BRIDGE_DAMAGE,
		/** @since 0.8 */
		HAZARD_BURST_PIPE,
		/** @since 0.8 */
		HAZARD_BURST_WATER_MAIN,
		/** @since 0.8 */
		HAZARD_CHILDREN_ON_ROAD,
		/** @since 0.8 */
		HAZARD_COLLAPSED_SEWER,
		/** @since 0.8 */
		HAZARD_CONVOY,
		/** @since 0.8 */
		HAZARD_CYCLISTS_ON_ROAD,
		/** @since 0.8 */
		HAZARD_DEEP_SNOW,
		/**
		 * @deprecated Use {@link #HAZARD_DEEP_SNOW} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_DEEP_SNOW_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_DIFFICULT_CONDITIONS,
		/**
		 * @deprecated Use {@link #HAZARD_DIFFICULT_CONDITIONS} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_DIFFICULT_CONDITIONS_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_EARTHQUAKE_DAMAGE,
		/** @since 0.8 */
		HAZARD_EMERGENCY_VEHICLE,
		/** @since 0.8 */
		HAZARD_EXTREMELY_HAZARDOUS_CONDITIONS,
		/**
		 * @deprecated Use {@link #HAZARD_EXTREMELY_HAZARDOUS_CONDITIONS} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_EXTREMELY_HAZARDOUS_CONDITIONS_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_FALLEN_POWER_CABLES,
		/** @since 0.8 */
		HAZARD_FALLEN_TREE,
		/** @since 0.8 */
		HAZARD_FLASH_FLOODS,
		/** @since 0.8 */
		HAZARD_FLOODING,
		/** @since 0.8 */
		HAZARD_FOREST_FIRE,
		/** @since 0.8 */
		HAZARD_FREEZING_RAIN,
		/**
		 * @deprecated Use {@link #HAZARD_FREEZING_RAIN} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_FREEZING_RAIN_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_FRESH_SNOW,
		/**
		 * @deprecated Use {@link #HAZARD_FRESH_SNOW} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_FRESH_SNOW_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_FUEL_ON_ROAD,
		/** @since 0.8 */
		HAZARD_GAS_LEAK,
		/** @since 0.8 */
		HAZARD_GRASS_FIRE,
		/** @since 0.8 */
		HAZARD_GRITTING_VEHICLE,
		/** @since 0.8 */
		HAZARD_HAZARDOUS_CONDITIONS,
		/**
		 * @deprecated Use {@link #HAZARD_HAZARDOUS_CONDITIONS} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_HAZARDOUS_CONDITIONS_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_HAZMAT,
		/** @since 0.8 */
		HAZARD_HIGH_SPEED_CHASE,
		/** @since 0.8 */
		HAZARD_HIGH_SPEED_EMERGENCY_VEHICLE,
		/** @since 0.8 */
		HAZARD_HOUSE_FIRE,
		/** @since 0.8 */
		HAZARD_ICE,
		/**
		 * @deprecated Use {@link #HAZARD_ICE} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_ICE_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_ICE_BUILDUP,
		/** @since 0.8 */
		HAZARD_ICE_LIKELY,
		/**
		 * @deprecated Use {@link #HAZARD_ICE_LIKELY} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_ICE_LIKELY_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_ICY_PATCHES,
		/**
		 * @deprecated Use {@link #HAZARD_ICY_PATCHES} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_ICY_PATCHES_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_ICY_PATCHES_LIKELY,
		/**
		 * @deprecated Use {@link #HAZARD_ICY_PATCHES_LIKELY} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_ICY_PATCHES_LIKELY_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_IMPASSABLE,
		/**
		 * @deprecated Use {@link #HAZARD_IMPASSABLE} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_IMPASSABLE_ABOVE_ELEVATION,
		/** @deprecated Use {@link #HAZARD_IMPASSABLE} and {@link TraffSupplementaryInfo.Type#S_VEHICLE_HEAVY} instead. */
		HAZARD_IMPASSABLE_OVER_WEIGHT,
		/** @since 0.8 */
		HAZARD_LANDSLIPS,
		/** @since 0.8 */
		HAZARD_LARGE_ANIMALS_ON_ROAD,
		/** @since 0.8 */
		HAZARD_LEAVES_ON_ROAD,
		/** @since 0.8 */
		HAZARD_LONG_LOAD,
		/** @since 0.8 */
		HAZARD_LOOSE_CHIPPINGS,
		/** @since 0.8 */
		HAZARD_LOOSE_SAND_ON_ROAD,
		/** @since 0.8 */
		HAZARD_MILITARY_CONVOY,
		/** @since 0.8 */
		HAZARD_MUD_ON_ROAD,
		/** @since 0.8 */
		HAZARD_MUD_SLIDE,
		/** @since 0.8 */
		HAZARD_OBJECT_ON_ROAD,
		/** @since 0.8 */
		HAZARD_OBJECTS_FALLING_FROM_MOVING_VEHICLE,
		/** @since 0.8 */
		HAZARD_OBSTRUCTION,
		/** @since 0.8 */
		HAZARD_OIL_ON_ROAD,
		/** @since 0.8 */
		HAZARD_OVERHEIGHT_LOAD,
		/** @since 0.8 */
		HAZARD_PACKED_SNOW,
		/**
		 * @deprecated Use {@link #HAZARD_PACKED_SNOW} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_PACKED_SNOW_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_PASSABLE,
		/**
		 * @deprecated Use {@link #HAZARD_PASSABLE} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_LOW_AREA} with a quantifier if needed.
		 */
		HAZARD_PASSABLE_BELOW_ELEVATION,
		/** @since 0.8 */
		HAZARD_PASSABLE_WITH_CARE,
		/**
		 * @deprecated Use {@link #HAZARD_PASSABLE_WITH_CARE} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_PASSABLE_WITH_CARE_ABOVE_ELEVATION,
		/**
		 * @deprecated Use {@link #HAZARD_PASSABLE_WITH_CARE} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_LOW_AREA} with a quantifier if needed.
		 */
		HAZARD_PASSABLE_WITH_CARE_BELOW_ELEVATION,
		/** @since 0.8 */
		HAZARD_PEOPLE_ON_ROAD,
		/** @since 0.8 */
		HAZARD_PROHIBITED_VEHICLE,
		/** @since 0.8 */
		HAZARD_RAIL_CRASH,
		/** @since 0.8 */
		HAZARD_RAIN_CHANGING_TO_SNOW,
		/** @since 0.8 */
		HAZARD_RECKLESS_DRIVER,
		/** @since 0.8 */
		HAZARD_ROAD_MARKING_WORK,
		/** @since 0.8 */
		HAZARD_ROCKFALLS,
		/** @since 0.8 */
		HAZARD_SERIOUS_FIRE,
		/** @since 0.8 */
		HAZARD_SEWER_OVERFLOW,
		/** @since 0.8 */
		HAZARD_SHED_LOAD,
		/** @since 0.8 */
		HAZARD_SIGHTSEERS_OBSTRUCTING_ACCESS,
		/** @since 0.8 */
		HAZARD_SLIPPERY_ROAD,
		/**
		 * @deprecated Use {@link #HAZARD_SLIPPERY_ROAD} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_SLIPPERY_ROAD_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_SLOW_MOVING_MAINTENANCE,
		/** @since 0.8 */
		HAZARD_SLOW_VEHICLE,
		/** @since 0.8 */
		HAZARD_SLUSH,
		/**
		 * @deprecated Use {@link #HAZARD_SLUSH} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_SLUSH_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_SNOW_AND_ICE_DEBRIS,
		/** @since 0.8 */
		HAZARD_SNOW_CHANGING_TO_RAIN,
		/** @since 0.8 */
		HAZARD_SNOW_DRIFTS,
		/**
		 * @deprecated Use {@link #HAZARD_SNOW_DRIFTS} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_SNOW_DRIFTS_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_SNOW_ON_ROAD,
		/**
		 * @deprecated Use {@link #HAZARD_SNOW_ON_ROAD} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_SNOW_ON_ROAD_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_SNOWPLOW,
		/** @since 0.8 */
		HAZARD_SPILLAGE,
		/** @since 0.8 */
		HAZARD_SPILLAGE_FROM_MOVING_VEHICLE,
		/** @since 0.8 */
		HAZARD_STORM_DAMAGE,
		/** @since 0.8 */
		HAZARD_SUBSIDENCE,
		/** @since 0.8 */
		HAZARD_SURFACE_WATER,
		/** @since 0.8 */
		HAZARD_TRACK_LAYING_VEHICLE,
		/**
		 * @deprecated Use {@link #HAZARD_IMPASSABLE} and
		 * {@link TraffSupplementaryInfo.Type#S_VEHICLE_WITH_TRAILER} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_TRAILER_IMPASSABLE_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_UNLIT_VEHICLE,
		/** @since 0.8 */
		HAZARD_UNPROTECTED_ACCIDENT,
		/** @since 0.8 */
		HAZARD_VEHICLE_STUCK_UNDER_BRIDGE,
		/** @since 0.8 */
		HAZARD_WET_ICY_ROADS,
		/**
		 * @deprecated Use {@link #HAZARD_WET_ICY_ROADS} instead,
		 * add {@link TraffSupplementaryInfo.Type#S_PLACE_HIGH_ALTITUDE} with a quantifier if needed.
		 */
		HAZARD_WET_ICY_ROADS_ABOVE_ELEVATION,
		/** @since 0.8 */
		HAZARD_WRONG_WAY_DRIVER,
		/** @since 0.8 */
		INCIDENT_ACCIDENT,
		/** @since 0.8 */
		INCIDENT_BROKEN_DOWN_BUS,
		/** @since 0.8 */
		INCIDENT_BROKEN_DOWN_HGV,
		/** @since 0.8 */
		INCIDENT_BROKEN_DOWN_VEHICLE,
		/** @since 0.8 */
		INCIDENT_BUS_ACCIDENT,
		/** @since 0.8 */
		INCIDENT_CHEMICAL_SPILLAGE,
		/** @since 0.8 */
		INCIDENT_CLEARANCE,
		/** @since 0.8 */
		INCIDENT_EARLIER_ACCIDENT,
		/** @since 0.8 */
		INCIDENT_FUEL_SPILLAGE,
		/** @since 0.8 */
		INCIDENT_GENERIC,
		/** @since 0.8 */
		INCIDENT_HAZMAT_ACCIDENT,
		/** @since 0.8 */
		INCIDENT_HGV_ACCIDENT,
		/** @since 0.8 */
		INCIDENT_INVESTIGATION,
		/** @since 0.8 */
		INCIDENT_JACKKNIFED_CARAVAN,
		/** @since 0.8 */
		INCIDENT_JACKKNIFED_HGV,
		/** @since 0.8 */
		INCIDENT_JACKKNIFED_TRAILER,
		/** @since 0.8 */
		INCIDENT_MULTI_VEHICLE_ACCIDENT,
		/** @since 0.8 */
		INCIDENT_OIL_SPILLAGE,
		/** @since 0.8 */
		INCIDENT_OVERTURNED_HGV,
		/** @since 0.8 */
		INCIDENT_OVERTURNED_VEHICLE,
		/** @since 0.8 */
		INCIDENT_RESCUE,
		/** @since 0.8 */
		INCIDENT_RUBBERNECKING,
		/** @since 0.8 */
		INCIDENT_SECONDARY_ACCIDENT,
		/** @since 0.8 */
		INCIDENT_SERIOUS_ACCIDENT,
		/** @since 0.8 */
		INCIDENT_SPUN_VEHICLE,
		/** @since 0.8 */
		INCIDENT_VEHICLE_FIRE,
		RESTRICTION_ACCESS_RESTRICTIONS_LIFTED,
		RESTRICTION_ALL_CARRIAGEWAYS_CLEARED,
		RESTRICTION_ALL_CARRIAGEWAYS_REOPENED,
		RESTRICTION_BATCH_SERVICE,
		RESTRICTION_BLOCKED,
		RESTRICTION_BLOCKED_AHEAD,
		RESTRICTION_CARRIAGEWAY_BLOCKED,
		RESTRICTION_CARRIAGEWAY_CLOSED,
		RESTRICTION_CLOSED,
		RESTRICTION_CLOSED_AHEAD,
		RESTRICTION_CONTRAFLOW,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_CONTRAFLOW_REMOVED,
		/** @since 0.8 */
		RESTRICTION_CONVOY_SERVICE_REQUIRED,
		RESTRICTION_ENTRY_BLOCKED,
		RESTRICTION_ENTRY_REOPENED,
		RESTRICTION_EXIT_BLOCKED,
		RESTRICTION_EXIT_REOPENED,
		RESTRICTION_INTERMITTENT_CLOSURES,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_LANE_BLOCKAGE_CLEARED,
		RESTRICTION_LANE_BLOCKED,
		RESTRICTION_LANE_CLOSED,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_LANE_CLOSURE_REMOVED,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_LANE_REGULATIONS_RESTORED,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_LANE_RESTRICTIONS_LIFTED,
		/** @since 0.8 */
		RESTRICTION_LET_EMERGENCY_SERVICES_PASS,
		/** @since 0.8 */
		RESTRICTION_MAX_AXLE_LOAD,
		/** @since 0.8 */
		RESTRICTION_MAX_HEIGHT,
		/** @since 0.8 */
		RESTRICTION_MAX_LENGTH,
		/** @since 0.8 */
		RESTRICTION_MAX_WEIGHT,
		/** @since 0.8 */
		RESTRICTION_MAX_WIDTH,
		/** @since 0.8 */
		RESTRICTION_NARROW_LANES,
		/** @since 0.8 */
		RESTRICTION_NO_OVERTAKING,
		/**
		 * @deprecated Use {@link #RESTRICTION_NO_OVERTAKING} and
		 * {@link TraffSupplementaryInfo.Type#S_VEHICLE_HEAVY} instead.
		 */
		RESTRICTION_NO_OVERTAKING_OVER_WEIGHT,
		RESTRICTION_OPEN,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_OVERTAKING_RESTRICTION_LIFTED,
		RESTRICTION_RAMP_BLOCKED,
		RESTRICTION_RAMP_CLOSED,
		RESTRICTION_RAMP_REOPENED,
		RESTRICTION_REDUCED_LANES,
		RESTRICTION_REOPENED,
		/** @since 0.8 */
		RESTRICTION_RESTRICTIONS,
		RESTRICTION_ROAD_CLEARED,
		/** @since 0.8 */
		RESTRICTION_SHOULDER_BLOCKED,
		RESTRICTION_SINGLE_ALTERNATE_LINE_TRAFFIC,
		/**
		 * @deprecated Use {@link #ENVIRONMENT_SMOG} instead; if restrictions are in place, add these
		 * separately.
		 */
		@Deprecated
		RESTRICTION_SMOG_ALERT,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_SMOG_ALERT_ENDED,
		RESTRICTION_SPEED_LIMIT,
		RESTRICTION_SPEED_LIMIT_LIFTED,
		/** @deprecated Use {@link #RESTRICTION_MAX_AXLE_LOAD} instead. */
		@Deprecated
		RESTRICTION_TEMP_MAX_AXLE_LOAD,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_TEMP_MAX_AXLE_LOAD_LIFTED,
		/** @deprecated Use {@link #RESTRICTION_MAX_HEIGHT} instead. */
		@Deprecated
		RESTRICTION_TEMP_MAX_HEIGHT,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_TEMP_MAX_HEIGHT_LIFTED,
		/** @deprecated Use {@link #RESTRICTION_MAX_LENGTH} instead. */
		@Deprecated
		RESTRICTION_TEMP_MAX_LENGTH,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_TEMP_MAX_LENGTH_LIFTED,
		/** @deprecated Use {@link #RESTRICTION_MAX_WEIGHT} instead. */
		@Deprecated
		RESTRICTION_TEMP_MAX_WEIGHT,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_TEMP_MAX_WEIGHT_LIFTED,
		/** @deprecated Use {@link #RESTRICTION_MAX_WIDTH} instead. */
		@Deprecated
		RESTRICTION_TEMP_MAX_WIDTH,
		/** @deprecated Use a cancellation event instead. */
		@Deprecated
		RESTRICTION_TEMP_MAX_WIDTH_LIFTED,
		/** @since 0.8 */
		RESTRICTION_TRAFFIC_DIRECTED_MANUALLY,
		/** @deprecated Use {@link TraffSupplementaryInfo.Type#S_LANE_USAGE_USE_SHOULDER} instead. */
		@Deprecated
		RESTRICTION_USE_SHOULDER,
		/** @since 0.8 */
		SECURITY_AIR_RAID,
		/** @since 0.8 */
		SECURITY_ALERT,
		/** @since 0.8 */
		SECURITY_BOMB_ALERT,
		/** @since 0.8 */
		SECURITY_CIVIL_EMERGENCY,
		/** @since 0.8 */
		SECURITY_EVACUATION,
		/** @since 0.8 */
		SECURITY_GUNFIRE,
		/** @since 0.8 */
		SECURITY_INCIDENT,
		/** @since 0.8 */
		SECURITY_TERRORIST_INCIDENT,
		/** @since 0.8 */
		TRANSPORT_CANCELLATIONS,
		/** @since 0.8 */
		TRANSPORT_DELAYED,
		/** @since 0.8 */
		TRANSPORT_FREE_SHUTTLE_SERVICE,
		/** @since 0.8 */
		TRANSPORT_MOVEABLE_BRIDGE_OPERATION,
		/** @since 0.8 */
		TRANSPORT_SERVICE_BOOKED_OUT,
		/** @since 0.8 */
		TRANSPORT_SERVICE_SUSPENDED,
		/** @since 0.8 */
		TRANSPORT_SERVICE_WITHDRAWN,
		/** @since 0.8 */
		TRANSPORT_SHUTTLE_SERVICE,
		/** @since 0.8 */
		TRANSPORT_SUBSTITUTE_SERVICE,
		/** @since 0.8 */
		WEATHER_BLIZZARD,
		/** @since 0.8 */
		WEATHER_BLOWING_DUST,
		/** @since 0.8 */
		WEATHER_BLOWING_SNOW,
		/** @since 0.8 */
		WEATHER_CROSSWIND,
		/** @since 0.8 */
		WEATHER_DAMAGING_HAIL,
		/** @since 0.8 */
		WEATHER_DENSE_FOG,
		/** @since 0.8 */
		WEATHER_EXTREME_COLD,
		/** @since 0.8 */
		WEATHER_EXTREME_HEAT,
		/** @since 0.8 */
		WEATHER_FOG,
		/** @since 0.8 */
		WEATHER_FREEZING_FOG,
		/** @since 0.8 */
		WEATHER_FROST,
		/** @since 0.8 */
		WEATHER_GALES,
		/** @since 0.8 */
		WEATHER_GUSTY_WIND,
		/** @since 0.8 */
		WEATHER_HAIL,
		/** @since 0.8 */
		WEATHER_HEAVY_FROST,
		/** @since 0.8 */
		WEATHER_HEAVY_RAIN,
		/** @since 0.8 */
		WEATHER_HEAVY_SNOWFALL,
		/** @since 0.8 */
		WEATHER_HURRICANE,
		/** @since 0.8 */
		WEATHER_INSECT_SWARMS,
		/** @since 0.8 */
		WEATHER_LOW_SUN_GLARE,
		/** @since 0.8 */
		WEATHER_PATCHY_FOG,
		/** @since 0.8 */
		WEATHER_RAIN,
		/** @since 0.8 */
		WEATHER_RAPID_TEMPERATURE_DROP,
		/** @since 0.8 */
		WEATHER_REDUCED_VISIBILITY,
		/** @since 0.8 */
		WEATHER_SANDSTORM,
		/** @since 0.8 */
		WEATHER_SHOWERS,
		/** @since 0.8 */
		WEATHER_SLEET,
		/** @since 0.8 */
		WEATHER_SMOKE_HAZARD,
		/** @since 0.8 */
		WEATHER_SNOWFALL,
		/** @since 0.8 */
		WEATHER_SPRAY_HAZARD,
		/** @since 0.8 */
		WEATHER_STORM,
		/** @since 0.8 */
		WEATHER_STRONG_WIND,
		/** @since 0.8 */
		WEATHER_THUNDERSTORM,
		/** @since 0.8 */
		WEATHER_TORNADO,
		/** @since 0.8 */
		WEATHER_WHITEOUT,
		/** @since 0.8 */
		WEATHER_WINTER_STORM
	}

	/**
	 * The event class.
	 */
	@Attribute(name="class")
	public final Class eventClass;

	/**
	 * The event type.
	 * 
	 * <p>The event type uniquely identifies the event and can be mapped to a string to be displayed to the user.
	 */
	@Attribute(required=false)
	public final Type type;

	/**
	 * The length of the affected route in meters.
	 */
	@Attribute(required=false)
	public final Integer length;

	/**
	 * Reserved for future use.
	 * 
	 * <p>The probability in percent for the forecast, common in weather forecasts.
	 * 
	 * <p>Do not use for events which are not forecasts.
	 */
	@Attribute(required=false)
	public final Integer probability;

	/**
	 * Quantifier for the event.
	 * 
	 * <p>Permissible data types and their meanings depend on the event type.
	 */
	@Transient
	public final Quantifier quantifier;

	/**
	 * Expected speed of travel.
	 * 
	 * <p>The speed at which vehicles can expect to pass through the affected stretch of road. This can either
	 * be a temporary speed limit or the average speed in practice: some events imply a particular
	 * interpretation; otherwise it refers to the lower of the two speeds.
	 */
	@Attribute(required=false)
	public final Integer speed;

	/**
	 * Supplementary information for the event.
	 */
	@Transient
	public final TraffSupplementaryInfo[] supplementaryInfos;

	private TraffEvent(Class eventClass, Type type, Integer length, Integer probability, Quantifier quantifier,
			Integer speed, TraffSupplementaryInfo[] supplementaryInfos) {
		super();
		this.eventClass = eventClass;
		this.type = type;
		this.length = length;
		this.probability = probability;
		this.quantifier = quantifier;
		this.speed = speed;
		this.supplementaryInfos = supplementaryInfos;
	}

	/**
	 * Constructor for XML deserialization.
	 * 
	 * @param eventClass
	 * @param type
	 * @param length
	 * @param probability
	 * @param qDimension
	 * @param qDuration
	 * @param qFrequency
	 * @param qInt
	 * @param qInts
	 * @param qPercent
	 * @param qSpeed
	 * @param qTemperature
	 * @param qTime
	 * @param qWeight
	 * @param speed
	 * @param supplementaryInfos
	 * @throws ParseException if an invalid quantifier is passed
	 */
	public TraffEvent(@Attribute(name="class") Class eventClass,
			@Attribute(name="type") Type type,
			@Attribute(name="length") Integer length,
			@Attribute(name="probability") Integer probability,
			@Attribute(name="q_dimension") Float qDimension,
			@Attribute(name="q_duration") String qDuration,
			@Attribute(name="q_frequency") Float qFrequency,
			@Attribute(name="q_int") Integer qInt,
			@Attribute(name="q_ints") String qInts,
			@Attribute(name="q_percent") Integer qPercent,
			@Attribute(name="q_speed") Integer qSpeed,
			@Attribute(name="q_temperature") Integer qTemperature,
			@Attribute(name="q_time") String qTime,
			@Attribute(name="q_weight") Float qWeight,
			@Attribute(name="speed") Integer speed,
			@ElementList(inline=true) List<TraffSupplementaryInfo> supplementaryInfos) throws ParseException {
		super();
		this.eventClass = eventClass;
		this.type = type;
		this.length = length;
		this.probability = probability;
		// FIXME parse quantifiers with units properly
		if (qDimension != null)
			this.quantifier = new DimensionQuantifier(qDimension);
		// TODO qDuration
		else if (qFrequency != null)
			this.quantifier = new FrequencyQuantifier(qFrequency);
		else if (qInt != null)
			this.quantifier = new IntQuantifier(qInt);
		// TODO qInts
		else if (qPercent != null)
			this.quantifier = new PercentQuantifier(qPercent);
		else if (qSpeed != null)
			this.quantifier = new SpeedQuantifier(qSpeed);
		else if (qTemperature != null)
			this.quantifier = new TemperatureQuantifier(qTemperature);
		else if (qTime != null)
			this.quantifier = new TimeQuantifier(Traff.getIsoDateFormat().parse(qTime));
		else if (qWeight != null)
			this.quantifier = new WeightQuantifier(qWeight);
		else
			this.quantifier = null;
		this.speed = speed;
		if (supplementaryInfos != null)
			this.supplementaryInfos = supplementaryInfos.toArray(new TraffSupplementaryInfo[]{});
		else
			this.supplementaryInfos = new TraffSupplementaryInfo[]{};
	}

	@Attribute(required=false, name="q_dimension")
	public Float getDimensionQuantifier() {
		if (quantifier instanceof DimensionQuantifier)
			return ((DimensionQuantifier) quantifier).value;
		else
			return null;
	}

	@Attribute(required=false, name="q_duration")
	public String getDurationQuantifier() {
		if (quantifier instanceof DurationQuantifier)
			return ((DurationQuantifier) quantifier).toString();
		else
			return null;
	}

	@Attribute(required=false, name="q_frequency")
	public Float getFrequencyQuantifier() {
		if (quantifier instanceof FrequencyQuantifier)
			return ((FrequencyQuantifier) quantifier).frequency;
		else
			return null;
	}

	@Attribute(required=false, name="q_int")
	public Integer getIntQuantifier() {
		if (quantifier instanceof IntQuantifier)
			return ((IntQuantifier) quantifier).value;
		else
			return null;
	}

	@Attribute(required=false, name="q_ints")
	public String getIntsQuantifier() {
		if (quantifier instanceof IntsQuantifier)
			return ((IntsQuantifier) quantifier).toString();
		else
			return null;
	}

	@Attribute(required=false, name="q_percent")
	public Integer getPercentQuantifier() {
		if (quantifier instanceof PercentQuantifier)
			return ((PercentQuantifier) quantifier).percent;
		else
			return null;
	}

	@Attribute(required=false, name="q_speed")
	public Integer getSpeedQuantifier() {
		if (quantifier instanceof SpeedQuantifier)
			return ((SpeedQuantifier) quantifier).speed;
		else
			return null;
	}

	@ElementList(inline=true, required=false)
	public List<TraffSupplementaryInfo> getSupplementaryInfosAsList() {
		if (supplementaryInfos == null)
			return null;
		else
			return Arrays.asList(supplementaryInfos);
	}

	@Attribute(required=false, name="q_temperature")
	public Integer getTemperatureQuantifier() {
		if (quantifier instanceof TemperatureQuantifier)
			return ((TemperatureQuantifier) quantifier).degreesCelsius;
		else
			return null;
	}

	@Attribute(required=false, name="q_time")
	public String getTimeQuantifier() {
		if (quantifier instanceof TimeQuantifier)
			return Traff.getIsoDateFormat().format(((TimeQuantifier) quantifier).time);
		else
			return null;
	}

	@Attribute(required=false, name="q_weight")
	public Float getWeightQuantifier() {
		if (quantifier instanceof WeightQuantifier)
			return ((WeightQuantifier) quantifier).weight;
		else
			return null;
	}

	/**
	 * Returns the event in TraFF XML format.
	 * 
	 * <p>Calling this method is equivalent to calling {@link #toXml(int)} with a zero argument. That is, the
	 * root element will not be indented. 
	 */
	@Deprecated
	public String toXml() {
		return toXml(0);
	}

	/**
	 * Returns the event in TraFF XML format.
	 * 
	 * @param indent Indentation for the root element
	 */
	@Deprecated
	public String toXml(int indent) {
		String indentStr = Traff.getIndentString(indent);
		StringBuilder builder = new StringBuilder();

		/* opening tag with attributes */
		builder.append(indentStr).append("<event");
		if (this.eventClass != null)
			builder.append(String.format(" class=\"%s\"", this.eventClass));
		if (this.type != null)
			builder.append(String.format(" type=\"%s\"", this.type));
		if (this.length != null)
			builder.append(String.format(" length=\"%d\"", this.length));
		if (this.probability != null)
			builder.append(String.format(" probability=\"%s\"", this.probability));
		if (quantifier != null)
			builder.append(quantifier.toAttribute());
		if (this.speed != null)
			builder.append(String.format(" speed=\"%d\"", this.speed));
		builder.append(">\n");

		/* supplementary information */
		for (TraffSupplementaryInfo supplementaryInfo : supplementaryInfos)
			builder.append(supplementaryInfo.toXml(indent + Traff.INDENT));

		/* closing tag */
		builder.append(indentStr).append("</event>\n");
		return builder.toString();
	}

	/**
	 * Used to build a new {@link TraffEvent}.
	 */
	public static class Builder {
		Class eventClass = null;
		Type type = null;
		Integer length = null;
		Integer probability = null;      /* reserved */
		Quantifier quantifier = null;
		Integer speed = null;
		// FIXME does a set alone reliably prevent duplicates?
		HashSet<TraffSupplementaryInfo> supplementaryInfos = new HashSet<TraffSupplementaryInfo>();

		public Builder() {
			super();
		}

		/**
		 * Initializes a new builder with the data of an existing {@link TraffEvent}.
		 */
		public Builder(TraffEvent event) {
			super();
			if (event == null)
				return;
			eventClass = event.eventClass;
			type = event.type;
			length = event.length;
			probability = event.probability;
			quantifier = event.quantifier;
			speed = event.speed;
			supplementaryInfos.addAll(Arrays.asList(event.supplementaryInfos));
		}

		public void addSupplementaryInfo(TraffSupplementaryInfo supplementaryInfo) {
			supplementaryInfos.add(supplementaryInfo);
		}

		public void addSupplementaryInfos(Collection<TraffSupplementaryInfo> supplementaryInfos) {
			this.supplementaryInfos.addAll(supplementaryInfos);
		}

		/**
		 * Builds a new {@link TraffEvent}.
		 * 
		 * <p>The builder will only build an event if its event class and type are both non-null. Otherwise, an
		 * {@link IllegalStateException} is thrown.
		 */
		public TraffEvent build() {
			if (eventClass == null)
				throw new IllegalStateException("event class must be specified");
			if (type == null)
				throw new IllegalStateException("event type must be specified");
			// TODO if quantifiers are set, check if the event supports them
			return new TraffEvent(eventClass, type, length, probability, quantifier, speed,
					supplementaryInfos.toArray(new TraffSupplementaryInfo[0]));
		}

		public void clearSupplementaryInfos() {
			supplementaryInfos.clear();
		}

		/**
		 * Merges the values of an event.
		 * 
		 * <p>Any non-null members of {@code event} will overwrite the corresponding data in the builder. If
		 * {@code event} has null members, the corresponding data in the builder remains untouched.
		 * 
		 * @param event The event to merge
		 */
		public void merge(TraffEvent event) {
			if (event == null)
				return;
			eventClass = event.eventClass;
			type = event.type;
			if (event.length != null)
				length = event.length;
			if (event.probability != null)
				probability = event.probability;
			if (event.quantifier != null)
				quantifier = event.quantifier;
			if (event.speed != null)
					speed = event.speed;
			supplementaryInfos.addAll(Arrays.asList(event.supplementaryInfos));
		}

		public void setEventClass(Class eventClass) {
			this.eventClass = eventClass;
		}

		public void setType(Type type) {
			this.type = type;
		}

		public void setLength(Integer length) {
			this.length = length;
		}

		public void setProbability(Integer probability) {
			this.probability = probability;
		}

		public void setQuantifier(Quantifier quantifier) {
			this.quantifier = quantifier;
		}

		public void setSpeed(Integer speed) {
			this.speed = speed;
		}
	}
}
