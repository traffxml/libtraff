/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;

/**
 * Encapsulates a TraFF message.
 *
 * <p>A message is the atomic element of traffic information, referring to a particular condition at a given
 * location.
 *
 * <p>Messages, including all members, are immutable. When changes are to be made, create a new message with the
 * updated data. 
 *
 * <p>New messages are generated using a {@link Builder}.
 */
@Root(strict=false, name="message")
@Order(elements={"merge", "location", "events"},
attributes={"id", "receive_time", "update_time", "expiration_time", "start_time", "end_time", "cancellation", "forecast", "urgency"})
public class TraffMessage implements Comparable<TraffMessage> {
	public static enum Urgency {
		INVALID,
		NORMAL,
		URGENT,
		X_URGENT
	}

	/**
	 * Unique alphanumeric identifier for the message.
	 * 
	 * <p>Every message in TraFF is identified by an alphanumeric identifier which incorporates the identifier
	 * of the source from which the message originated. No two messages with the same identifier may exist at
	 * the same time, although this must be enforced by the application.
	 * 
	 * <p>After a message expires, its identifier can be re-used for a new message. If a message has been
	 * explicitly canceled or replaced, its identifier should not be re-used until the last expiration time
	 * ever associated with this ID has elapsed. This is to avoid confusing receivers which may have missed
	 * some updates and may therefore assume the old ID to still be valid.
	 */
	@Attribute
	public final String id;

	/**
	 * A timestamp indicating at what time the message was first received by the source.
	 * 
	 * <p>Sources are expected to keep this attribute stable across all updates.
	 */
	@Transient
	public final Date receiveTime;

	/**
	 * A timestamp indicating at what time the last update to this message was received by the source.
	 */
	@Transient
	public final Date updateTime;

	/**
	 * A timestamp indicating when the message will expire if not updated.
	 * 
	 * <p>An expired message should be deleted or no longer be considered current. If {@code endTime} is
	 * specified and is longer than {@code expirationTime}, {@code endTime} should be used to govern
	 * expiration and this attribute ignored. The expiration time governs how consumers will treat a message
	 * if they do not receive any further updates, e.g. because they lose the data connection. Sources should
	 * therefore choose the expiration time carefully: if a situation is not expected to resolve in a certain
	 * period of time, the expiration time should not be shorter. Conversely, if a situation is likely to
	 * have resolved within a certain period of time, the expiration time should not be longer than that.
	 */
	@Transient
	public final Date expirationTime;

	/**
	 * A timestamp indicating when the condition is expected to begin.
	 * 
	 * <p>Null indicates that the condition has already begun.
	 */
	@Transient
	public final Date startTime;

	/**
	 * A timestamp indicating how long the condition is expected to last.
	 */
	@Transient
	public final Date endTime;

	/**
	 * Whether this message is a cancellation.
	 * 
	 * <p>If true, this message is a cancellation, indicating that existing messages with the same ID should be
	 * deleted or no longer considered current. All other members of a cancellation message should be
	 * ignored.
	 */
	@Attribute(required=false, name="cancellation")
	public final boolean isCancellation;

	/**
	 * Whether the message is a forecast.
	 * 
	 * <p>If true, this message describes a situation expected for the future. If false, it describes a current
	 * situation.
	 */
	@Attribute(required=false, name="forecast")
	public final boolean isForecast;

	/**
	 * The urgency of the message.
	 * 
	 * <p>This allows the consumer to decide how the message should be presented to the user. X_URGENT messages
	 * should be presented immediately, URGENT messages may be delayed and NORMAL messages may not need to be
	 * presented at all (e.g. for delays, it is sufficient to route the driver around them).
	 */
	@Attribute(required=false)
	public final Urgency urgency;

	/**
	 * Messages replaced by the current message.
	 * 
	 * <p>Each entry is the ID of another message which is being replaced by the current one. The current
	 * message ID is not repeated here.
	 */
	@Transient
	public final String[] replaces;

	/**
	 * The location to which the message refers.
	 */
	@Element(required=false)
	public final TraffLocation location;

	/**
	 * The events associated with the message.
	 */
	@Transient
	public final TraffEvent[] events;

	/**
	 * Reads an XML input stream and returns the resulting {@code TraffMessage}.
	 * 
	 * @param inputStream An input stream which holds a valid TraFF message
	 * 
	 * @return An {@code TraffMessage} representing the data in the input stream
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static TraffMessage read(InputStream inputStream) throws Exception {
		TraffMessage res = Traff.XML_SERIALIZER.read(TraffMessage.class, inputStream);
		return res;
	}

	/**
	 * Reads an XML string and returns the resulting {@code TraffMessage}.
	 * 
	 * <p>This is a convenience wrapper around {@link #read(InputStream)}.
	 * 
	 * @param xml A string which holds a valid TraFF message
	 * 
	 * @return An {@code TraffMessage} representing the data in the string
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static TraffMessage read(String xml) throws Exception {
		return read(new ByteArrayInputStream(xml.getBytes()));
	}

	private TraffMessage(String id, Date receiveTime, Date updateTime, Date expirationTime, Date startTime,
			Date endTime, boolean isCancellation, boolean isForecast, Urgency urgency, String[] replaces, TraffLocation location,
			TraffEvent[] events) {
		super();
		this.id = id;
		this.receiveTime = receiveTime;
		this.updateTime = updateTime;
		this.expirationTime = expirationTime;
		this.startTime = startTime;
		this.endTime = endTime;
		this.isCancellation = isCancellation;
		this.isForecast = isForecast;
		this.urgency = urgency;
		this.replaces = replaces;
		this.location = location;
		this.events = events;
	}

	/**
	 * Constructor for XML deserialization.
	 * 
	 * @param id
	 * @param receiveTime
	 * @param updateTime
	 * @param expirationTime
	 * @param startTime
	 * @param endTime
	 * @param isCancellation
	 * @param isForecast
	 * @param urgency
	 * @param replaces
	 * @param location
	 * @param events
	 * @throws ParseException if one of the timestamps is invalid
	 */
	public TraffMessage(@Attribute(name="id") String id,
			@Attribute(name="receive_time") String receiveTime,
			@Attribute(name="update_time") String updateTime,
			@Attribute(name="expiration_time") String expirationTime,
			@Attribute(name="start_time") String startTime,
			@Attribute(name="end_time") String endTime,
			@Attribute(name="cancellation") Boolean isCancellation,
			@Attribute(name="forecast") Boolean isForecast,
			@Attribute(name="urgency") Urgency urgency,
			@ElementList(inline=true) @Path("merge") List<ReplacedId> replaces,
			@Element(name="location") TraffLocation location,
			@ElementList(inline=true) @Path("events") List<TraffEvent> events) throws ParseException {
		super();
		DateFormat format = Traff.getIsoDateFormat();
		this.id = id;
		// TODO can we parse ISO8601 directly to a Date argument?
		this.receiveTime = format.parse(receiveTime);
		this.updateTime = format.parse(updateTime);
		if (expirationTime != null)
			this.expirationTime = format.parse(expirationTime);
		else
			this.expirationTime = null;
		if (startTime != null)
			this.startTime = format.parse(startTime);
		else
			this.startTime = null;
		if (endTime != null)
			this.endTime = format.parse(endTime);
		else
			this.endTime = null;
		if (isCancellation != null)
			this.isCancellation = isCancellation;
		else
			this.isCancellation = false;
		if (isForecast != null)
			this.isForecast = isForecast;
		else
			this.isForecast = false;
		if (urgency != null)
			this.urgency = urgency;
		else
			this.urgency = Urgency.NORMAL;
		if (replaces != null) {
			this.replaces = new String[replaces.size()];
			for (int i = 0; i < replaces.size(); i++)
				this.replaces[i] = replaces.get(i).id;
		} else
			this.replaces = new String[0];
		this.location = location;
		if (events != null)
			this.events = events.toArray(new TraffEvent[]{});
		else
			this.events = new TraffEvent[]{};
	}

	/**
	 * Compares this message with the specified message for order.
	 *
	 * <p>This is the default comparison, which merely examines the message ID and is useful for keeping
	 * messages in a set or map (preventing duplicates) but may not be suitable for display.
	 * 
	 * @return a negative integer, zero, or a positive integer as this message is less than, equal to, or
	 * greater than the specified message.
	 */
	@Override
	public int compareTo(TraffMessage o) {
		return this.id.compareTo(o.id);
	}

	@Attribute(name="receive_time")
	public String getReceiveTimeAsIsoString() {
		return Traff.getIsoDateFormat().format(receiveTime);
	}

	@Attribute(name="update_time")
	public String getUpdateTimeAsIsoString() {
		return Traff.getIsoDateFormat().format(updateTime);
	}

	@Attribute(required=false, name="expiration_time")
	public String getExpirationTimeAsIsoString() {
		if (expirationTime == null)
			return null;
		else
			return Traff.getIsoDateFormat().format(expirationTime);
	}

	@ElementList(required=false, inline=true)
	@Path("merge")
	public List<ReplacedId> getReplacesAsList() {
		if (replaces == null)
			return null;
		else {
			List<ReplacedId> res = new ArrayList<ReplacedId>();
			for (String id : replaces)
				res.add(new ReplacedId(id));
			return res;
		}
	}

	@Attribute(required=false, name="start_time")
	public String getStartTimeAsIsoString() {
		if (startTime == null)
			return null;
		else
			return Traff.getIsoDateFormat().format(startTime);
	}

	@Attribute(required=false, name="end_time")
	public String getEndTimeAsIsoString() {
		if (endTime == null)
			return null;
		else
			return Traff.getIsoDateFormat().format(endTime);
	}

	@ElementList(required=false, inline=true)
	@Path("events")
	public List<TraffEvent> getEventsAsList() {
		if (events == null)
			return null;
		else
			return Arrays.asList(events);
	}

	/**
	 * Gets the time after which this message effectively expires.
	 * 
	 * <p>The effective expiration time is the latest non-null value of {@link #expirationTime},
	 * {@link #startTime} and {@link #endTime}.
	 * 
	 * @return The effective expiration time for the message.
	 */
	public Date getEffectiveExpirationTime() {
		Date then;
		Date expiration = this.expirationTime;

		then = this.startTime;
		if ((expiration == null) || ((then != null) && (expiration.before(then))))
			expiration = then;

		then = this.endTime;
		if ((expiration == null) || ((then != null) && (expiration.before(then))))
			expiration = then;
		return expiration;
	}

	/**
	 * Whether the message has expired.
	 * 
	 * <p>A message is considered to have expired if its effective expiration time (as returned by
	 * {@link #getEffectiveExpirationTime()} refers to a point in time before {@code now}.
	 * 
	 * @param now The reference time to compare to (usually current time)
	 * 
	 * @return True if the message has expired, false if not.
	 */
	public boolean isExpired(Date now) {
		Date expiration = getEffectiveExpirationTime();

		return expiration.before(now);
	}

	/**
	 * Whether a message replaces another.
	 * 
	 * <p>A message replaces another if both have identical IDs, or if the ID of the old message appears in the
	 * {@link #replaces} member of the new one.
	 * 
	 * <p>This assumes that the message for which this method is called was received after {@code message}. If
	 * this is not the case, the behavior is undefined.
	 * 
	 * @param message The old (potentially replaced) message
	 * 
	 * @return True if the current message replaces {@code message}, false if not
	 */
	public boolean overrides(TraffMessage message) {
		if (this.id.equals(message.id))
			return true;
		for (String replacedId : this.replaces)
			if (replacedId.equals(message.id))
				return true;
		return false;
	}

	/**
	 * Returns the message as a string in TraFF XML format.
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public String toXml() throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		write(out);
		return out.toString();
	}

	/**
	 * Returns the message as a string in TraFF XML format.
	 * 
	 * @deprecated Use {@link #toXml()} instead.
	 * 
	 * @param indent Ignored
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	@Deprecated
	public String toXml(int indent) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		write(out);
		return out.toString();
	}

	/**
	 * Writes a message to an XML stream.
	 * 
	 * @param out The stream to which the XML output will be written
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public void write(OutputStream out) throws Exception {
		Traff.XML_SERIALIZER.write(this, out);
	}

	/**
	 * Returns a copy of an existing message with a new {@code updateTime}.
	 * 
	 * @param old The existing message
	 * @param updateTime The new update time
	 * 
	 * @return A shallow copy of {@code old}, with the {@code updateTime} member changed as specified
	 */
	public TraffMessage getUpdatedMessage(TraffMessage old, Date updateTime) {
		return new TraffMessage(old.id, old.receiveTime, updateTime, old.expirationTime, old.startTime,
				old.endTime, old.isCancellation, old.isForecast, old.urgency, old.replaces, old.location,
				old.events);
	}

	/**
	 * Used to build a new {@link TraffMessage}.
	 */
	public static class Builder {
		String id = null;
		Date receiveTime = null;
		Date updateTime = null;
		Date expirationTime = null;
		Date startTime = null;
		Date endTime = null;
		boolean isCancellation = false;
		boolean isForecast = false;
		Urgency urgency = Urgency.NORMAL;
		HashSet<String> replaces = new HashSet<String>();
		TraffLocation location = null;
		HashSet<TraffEvent> events = new HashSet<TraffEvent>();

		public Builder() {
			super();
		}

		/**
		 * Initializes a new builder with the data of an existing {@link TraffMessage}.
		 */
		public Builder(TraffMessage message) {
			super();
			if (message == null)
				return;
			id = message.id;
			receiveTime = message.receiveTime;
			updateTime = message.updateTime;
			expirationTime = message.expirationTime;
			startTime = message.startTime;
			endTime = message.endTime;
			isCancellation = message.isCancellation;
			isForecast = message.isForecast;
			replaces.addAll(Arrays.asList(message.replaces));
			location = message.location;
			events.addAll(Arrays.asList(message.events));
		}

		public void addEvent(TraffEvent event) {
			events.add(event);
		}

		public void addEvents(Collection<TraffEvent> events) {
			this.events.addAll(events);
		}

		public void addReplaces(String id) {
			replaces.add(id);
		}

		public void addReplaces(Collection<String> ids) {
			replaces.addAll(ids);
		}

		/**
		 * Builds a new {@link TraffMessage}.
		 * 
		 * <p>The builder performs a basic consistency check on the message. The ID must be non-null and not
		 * empty, and at least receive time and update time must be non-null. Unless the message is a
		 * cancellation, it must have a location and at least one event. If any of these criteria is not
		 * satisfied, an {@link IllegalStateException} is thrown.
		 */
		public TraffMessage build() {
			if ((id == null) || id.isEmpty())
				throw new IllegalStateException("id must be specified");
			if ((receiveTime == null) || (updateTime == null))
				throw new IllegalStateException("receive time and update time must be specified");
			if (!isCancellation) {
				if (location == null)
					throw new IllegalStateException("location must be specified unless the event is a cancellation");
				if (events.isEmpty())
					throw new IllegalStateException("At least one event must be specified unless the event is a cancellation");
			}
			return new TraffMessage(id, receiveTime, updateTime, expirationTime, startTime, endTime,
					isCancellation, isForecast, urgency, replaces.toArray(new String[0]), location,
					events.toArray(new TraffEvent[0]));
		}

		public void clearEvents() {
			events.clear();
		}

		public void clearReplaces() {
			replaces.clear();
		}

		public void setId(String id) {
			this.id = id;
		}

		public void setReceiveTime(Date receiveTime) {
			this.receiveTime = receiveTime;
		}

		public void setUpdateTime(Date updateTime) {
			this.updateTime = updateTime;
		}

		public void setExpirationTime(Date expirationTime) {
			this.expirationTime = expirationTime;
		}

		public void setStartTime(Date startTime) {
			this.startTime = startTime;
		}

		public void setEndTime(Date endTime) {
			this.endTime = endTime;
		}

		public void setCancellation(boolean isCancellation) {
			this.isCancellation = isCancellation;
		}

		public void setForecast(boolean isForecast) {
			this.isForecast = isForecast;
		}

		public void setUrgency(Urgency urgency) {
			this.urgency = urgency;
		}

		public void setLocation(TraffLocation location) {
			this.location = location;
		}
	}

	@Root(strict=false, name="replaces")
	public static class ReplacedId {
		@Attribute
		public final String id;

		public ReplacedId(@Attribute(name="id") String id) {
			super();
			this.id = id;
		}
	}
}
