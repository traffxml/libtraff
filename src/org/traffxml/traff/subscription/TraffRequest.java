/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff.subscription;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;
import org.traffxml.traff.Traff;

@Root(strict=false, name="request")
public class TraffRequest {
	public static enum Operation {
		GET_CAPABILITIES,
		SUBSCRIBE,
		UNSUBSCRIBE,
		CHANGE,
		/** @deprecated Reserved for future use */
		@Deprecated
		PUSH,
		POLL,
		HEARTBEAT
	}

	/** The operation to be carried out. */
	@Attribute(required=false)
	public final Operation operation;

	/** 
	 * The identifier of the subscription to which the request refers.
	 * 
	 * <p>Null if the {@code operation} is {@link Operation#SUBSCRIBE}, mandatory otherwise.
	 */
	@Attribute(required=false, name="subscription_id")
	public final String subscriptionId;

	/**
	 * The filter list for a new or changed subscription.
	 * 
	 * <p>Null unless the {@code operation} is {@link Operation#SUBSCRIBE} or {@link Operation#CHANGE}.
	 */
	@Transient
	public final FilterItem[] filterList;

	/**
	 * Reads an XML input stream and returns the resulting {@code TraffRequest}.
	 * 
	 * @param inputStream An input stream which holds a valid TraFF request
	 * 
	 * @return An {@code TraffRequest} representing the data in the input stream
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static TraffRequest read(InputStream inputStream) throws Exception {
		TraffRequest res = Traff.XML_SERIALIZER.read(TraffRequest.class, inputStream);
		return res;
	}

	/**
	 * Reads an XML string and returns the resulting {@code TraffRequest}.
	 * 
	 * <p>This is a convenience wrapper around {@link #read(InputStream)}.
	 * 
	 * @param xml A string which holds a valid TraFF request
	 * 
	 * @return An {@code TraffRequest} representing the data in the string
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static TraffRequest read(String xml) throws Exception {
		return read(new ByteArrayInputStream(xml.getBytes()));
	}

	// FIXME filter_list child element as container for filter elements
	public TraffRequest(@Attribute(name="operation") Operation operation,
			@Attribute(name="subscription_id") String subscriptionId,
			@ElementList(inline=true) @Path("filter_list") List<FilterItem> filterList) {
		super();
		this.operation = operation;
		this.subscriptionId = subscriptionId;
		if (filterList != null)
			this.filterList = filterList.toArray(new FilterItem[]{});
		else
			this.filterList = new FilterItem[]{};
	}

	@ElementList(inline=true, required=false)
	@Path("filter_list")
	public List<FilterItem> getFilterListAsList() {
		if (filterList == null)
			return null;
		else
			return Arrays.asList(filterList);
	}

	/**
	 * Returns the request as a string in TraFF XML format.
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public String toXml() throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		write(out);
		return out.toString();
	}

	/**
	 * Writes a request to an XML stream.
	 * 
	 * @param out The stream to which the XML output will be written
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public void write(OutputStream out) throws Exception {
		Traff.XML_SERIALIZER.write(this, out);
	}
}
