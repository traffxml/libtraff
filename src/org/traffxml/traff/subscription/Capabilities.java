/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff.subscription;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.traffxml.traff.Traff;

@Root(strict=false, name="capabilities")
public class Capabilities {
	/** The minimum TraFF version a peer must support to be able to interact with this peer. */
	@Attribute(required=false, name="min_version")
	public final Integer minVersion;

	/**
	 * The minimum TraFF version a peer must support in order to make full use of all the features offered by
	 * this peer.
	 */
	@Attribute(required=false, name="target_version")
	public final Integer targetVersion;

	/**
	 * Reads an XML input stream and returns the resulting {@code Capabilities}.
	 * 
	 * @param inputStream An input stream which holds a valid {@code capabilities} element
	 * 
	 * @return A {@code Capabilities} instance representing the data in the input stream
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static Capabilities read(InputStream inputStream) throws Exception {
		Capabilities res = Traff.XML_SERIALIZER.read(Capabilities.class, inputStream);
		return res;
	}

	/**
	 * Reads an XML string and returns the resulting {@code Capabilities}.
	 * 
	 * <p>This is a convenience wrapper around {@link #read(InputStream)}.
	 * 
	 * @param xml A string which holds a valid {@code capabilities} element
	 * 
	 * @return A {@code Capabilities} instance representing the data in the string
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static Capabilities read(String xml) throws Exception {
		return read(new ByteArrayInputStream(xml.getBytes()));
	}

	public Capabilities(@Attribute(name="min_version") Integer minVersion,
			@Attribute(name="target_version") Integer targetVersion) {
		super();
		this.minVersion = minVersion;
		this.targetVersion = targetVersion;
	}

	/**
	 * Returns the capabilities as a string in XML format.
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public String toXml() throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		write(out);
		return out.toString();
	}

	/**
	 * Writes a filter list to an XML stream.
	 * 
	 * @param out The stream to which the XML output will be written
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public void write(OutputStream out) throws Exception {
		Traff.XML_SERIALIZER.write(this, out);
	}
}
