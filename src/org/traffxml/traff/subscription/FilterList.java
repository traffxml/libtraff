/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff.subscription;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;
import org.traffxml.traff.Traff;

/**
 * Encapsulates a list of filter items.
 * 
 * <p>The main purpose if this class is serialization and deserialization of a filter list on its own. Other use
 * cases typically use an array or list of {@link FilterItem} instances directly.
 */
@Root(strict=false, name="filter_list")
public class FilterList {
	/**
	 * The list of filter items.
	 */
	@Transient
	public final FilterItem[] filterList;

	/**
	 * Reads an XML input stream and returns the resulting {@code FilterList}.
	 * 
	 * @param inputStream An input stream which holds a valid TraFF filter list
	 * 
	 * @return A {@code FilterList} representing the data in the input stream
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static FilterList read(InputStream inputStream) throws Exception {
		FilterList res = Traff.XML_SERIALIZER.read(FilterList.class, inputStream);
		return res;
	}

	/**
	 * Reads an XML string and returns the resulting {@code FilterList}.
	 * 
	 * <p>This is a convenience wrapper around {@link #read(InputStream)}.
	 * 
	 * @param xml A string which holds a valid TraFF filter list
	 * 
	 * @return A {@code FilterList} representing the data in the string
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static FilterList read(String xml) throws Exception {
		return read(new ByteArrayInputStream(xml.getBytes()));
	}

	public FilterList(@ElementList(inline=true) List<FilterItem> filterList) {
		super();
		if (filterList != null)
			this.filterList = filterList.toArray(new FilterItem[]{});
		else
			this.filterList = new FilterItem[]{};
	}

	@ElementList(inline=true, required=false)
	public List<FilterItem> getFilterListAsList() {
		if (filterList == null)
			return null;
		else
			return Arrays.asList(filterList);
	}

	/**
	 * Returns the capabilities as a string in XML format.
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public String toXml() throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		write(out);
		return out.toString();
	}

	/**
	 * Writes a filter list to an XML stream.
	 * 
	 * @param out The stream to which the XML output will be written
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public void write(OutputStream out) throws Exception {
		Traff.XML_SERIALIZER.write(this, out);
	}
}
