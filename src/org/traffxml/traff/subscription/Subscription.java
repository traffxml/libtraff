/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff.subscription;

import java.util.List;

/**
 * Describes a TraFF subscription.
 * 
 * <p>A subscription has an ID, by which it is identified, and a filter list specifying criteria for
 * the messages the subscriber wishes to receive.
 */
public class Subscription {
	/**
	 * The identifier for the subscription.
	 * 
	 * <p>The ID is chosen by the source. The only requirement is that it must be unique for each new
	 * subscription, but it does not follow a fixed format.
	 */
	public final String id;
	
	/** The filter list. */
	public final List<FilterItem> filterList;

	public Subscription(String id, List<FilterItem> filterList) {
		super();
		this.id = id;
		this.filterList = filterList;
	}
}
