/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff.subscription;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.traffxml.traff.Traff;
import org.traffxml.traff.TraffFeed;

@Root(strict=false, name="response")
public class TraffResponse {
	public static enum Status {
		OK,
		INVALID,
		SUBSCRIPTION_REJECTED,
		NOT_COVERED,
		PARTIALLY_COVERED,
		SUBSCRIPTION_UNKNOWN,
		/** @deprecated Reserved for future use. */
		@Deprecated
		PUSH_REJECTED
	}

	/** The status of the operation to which this response refers. */
	@Attribute(name="status")
	public final Status status;

	/** 
	 * The identifier of the subscription to which the response refers.
	 * 
	 * <p>Mandatory for a new subscription, null otherwise.
	 */
	@Attribute(required=false, name="subscription_id")
	public final String subscriptionId;

	/** 
	 * The time in seconds after which a subscription will time out if there is no activity.
	 * 
	 * <p>A value of 0 indicates that the subscription will never time out.
	 * 
	 * <p>Mandatory for a new subscription on certain transport channels, optional on other channels, null otherwise.
	 */
	@Attribute(required=false, name="timeout")
	public final Integer timeout;

	/**
	 * The capabilities of a peer.
	 * 
	 * <p>Null unless sent in response to a get capabilities operation.
	 */
	@Element(required=false)
	public final Capabilities capabilities;

	/**
	 * A feed of messages.
	 * 
	 * <p>Null unless sent in response to a poll operation.
	 */
	@Element(required=false)
	public final TraffFeed feed;

	/**
	 * Reads an XML input stream and returns the resulting {@code TraffRequest}.
	 * 
	 * @param inputStream An input stream which holds a valid TraFF request
	 * 
	 * @return An {@code TraffRequest} representing the data in the input stream
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static TraffResponse read(InputStream inputStream) throws Exception {
		TraffResponse res = Traff.XML_SERIALIZER.read(TraffResponse.class, inputStream);
		return res;
	}

	/**
	 * Reads an XML string and returns the resulting {@code TraffRequest}.
	 * 
	 * <p>This is a convenience wrapper around {@link #read(InputStream)}.
	 * 
	 * @param xml A string which holds a valid TraFF request
	 * 
	 * @return An {@code TraffRequest} representing the data in the string
	 * 
	 * @throws Exception if any errors occur during deserialization
	 */
	public static TraffResponse read(String xml) throws Exception {
		return read(new ByteArrayInputStream(xml.getBytes()));
	}

	public TraffResponse(@Attribute(name="status") Status status,
			@Attribute(name="subscription_id") String subscriptionId,
			@Attribute(name="timeout") Integer timeout,
			@Element(name="capabilities") Capabilities capabilities,
			@Element(name="feed") TraffFeed feed) {
		super();
		this.status = status;
		this.subscriptionId = subscriptionId;
		this.timeout = timeout;
		this.capabilities = capabilities;
		this.feed = feed;
	}

	/**
	 * Returns the request as a string in TraFF XML format.
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public String toXml() throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		write(out);
		return out.toString();
	}

	/**
	 * Writes a request to an XML stream.
	 * 
	 * @param out The stream to which the XML output will be written
	 * 
	 * @throws Exception if any errors occur during serialization
	 */
	public void write(OutputStream out) throws Exception {
		Traff.XML_SERIALIZER.write(this, out);
	}
}
