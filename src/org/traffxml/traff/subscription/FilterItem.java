/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff.subscription;

import java.text.ParseException;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffLocation.RoadClass;

/**
 * Describes a filter item for a TraFF subscription.
 * 
 * <p>A filter item limits the scope of messages returned as part of a subscription. It can specify a
 * minimum road class, a bounding box, or both. A subscription can have multiple filter items; a
 * message will be returned if it matches at least one filter item.
 */
@Root(strict=false, name="filter")
public class FilterItem {
	/** The bounding box. */
	public final BoundingBox bbox;

	/** The minimum road class. */
	@Attribute(required=false, name="min_road_class")
	public final TraffLocation.RoadClass minRoadClass;

	public FilterItem(BoundingBox bbox, RoadClass minRoadClass) {
		super();
		this.bbox = bbox;
		this.minRoadClass = minRoadClass;
	}

	public FilterItem(@Attribute(name="bbox") String bbox,
			@Attribute(name="min_road_class") RoadClass minRoadClass) throws ParseException {
		this((bbox == null) ? null : new BoundingBox(bbox), minRoadClass);
	}

	@Attribute(required=false, name="bbox")
	public String getBoundingBox() {
		return String.format("%f %f %f %f", bbox.minLat, bbox.minLon, bbox.maxLat, bbox.maxLon);
	}

	@Override
	public int hashCode() {
		int res = 0;
		if (minRoadClass != null)
			res ^= minRoadClass.name().hashCode();
		if (bbox != null)
			res ^= bbox.hashCode();
		return res;
	}
}
