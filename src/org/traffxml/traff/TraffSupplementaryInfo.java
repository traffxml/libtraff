/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff;

import java.text.ParseException;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Transient;

/**
 * Encapsulates supplementary information associated with a {@link TraffEvent}.
 * 
 * <p>Supplementary information may refer to vehicles or places to which the event applies, to warnings or
 * instructions.
 */
@Root(strict=false, name="supplementary_info")
@Order(attributes={"class", "type"})
public class TraffSupplementaryInfo {
	public static enum Class {
		INVALID,
		/** @deprecated Reserved for future use. */
		@Deprecated
		COURTESY,
		/** @deprecated Reserved for future use. */
		@Deprecated
		DIRECTION,
		/** @since 0.8 */
		DIVERSION,
		/** @since 0.8 */
		INSTRUCTION,
		/** @since 0.8 */
		LANE_USAGE,
		PLACE,
		/** @deprecated Reserved for future use. */
		@Deprecated
		POSITION,
		/** @since 0.8 */
		QUALIFIER,
		/** @since 0.8 */
		REASON,
		/** @since 0.8 */
		SPEED,
		/** @deprecated Reserved for future use. */
		@Deprecated
		SUGGESTION,
		TENDENCY,
		/** @since 0.8 */
		TRAFFIC,
		VEHICLE,
		/** @since 0.8 */
		WARNING,
		/** @since 0.8 */
		WINTER
	}

	public static enum Type {
		INVALID,
		/** @since 0.8 */
		S_DIVERSION_AVOID_AREA,
		/** @since 0.8 */
		S_DIVERSION_FOLLOW_DIVERSION_SIGNS,
		/** @since 0.8 */
		S_DIVERSION_FOLLOW_MARKERS,
		/** @since 0.8 */
		S_DIVERSION_IGNORE_SIGNS,
		/** @since 0.8 */
		S_DIVERSION_LOCAL,
		/** @since 0.8 */
		S_INSTRUCTION_APPROACH_WITH_CARE,
		/** @since 0.8 */
		S_INSTRUCTION_AVOID_GAPS,
		/** @since 0.8 */
		S_INSTRUCTION_AVOID_TRAVELING,
		/** @since 0.8 */
		S_INSTRUCTION_AWAIT_ESCORT,
		/** @since 0.8 */
		S_INSTRUCTION_AWAIT_PATROL,
		/** @since 0.8 */
		S_INSTRUCTION_CAREFUL,
		/** @since 0.8 */
		S_INSTRUCTION_CROSS_WITH_CARE,
		/** @since 0.8 */
		S_INSTRUCTION_EXTREME_CAUTION,
		/** @since 0.8 */
		S_INSTRUCTION_FOLLOW_PRECEDING_VEHICLE,
		/** @since 0.8 */
		S_INSTRUCTION_INCREASE_DISTANCE,
		/** @since 0.8 */
		S_INSTRUCTION_KEEP_DISTANCE,
		/** @since 0.8 */
		S_INSTRUCTION_LEAVE_VEHICLE,
		/** @since 0.8 */
		S_INSTRUCTION_NO_BURNING_OBJECTS,
		/** @since 0.8 */
		S_INSTRUCTION_NO_NAKED_FLAMES,
		/** @since 0.8 */
		S_INSTRUCTION_NO_SLOWDOWN,
		/** @since 0.8 */
		S_INSTRUCTION_NO_SMOKING,
		/** @since 0.8 */
		S_INSTRUCTION_OBSERVE_SIGNALS,
		/** @since 0.8 */
		S_INSTRUCTION_OBSERVE_SIGNS,
		/** @since 0.8 */
		S_INSTRUCTION_OVERTAKE_WITH_CARE,
		/** @since 0.8 */
		S_INSTRUCTION_PULL_OVER,
		/** @since 0.8 */
		S_INSTRUCTION_SHUT_OFF_VENTS,
		/** @since 0.8 */
		S_INSTRUCTION_STAY_IN_VEHICLE,
		/** @since 0.8 */
		S_INSTRUCTION_STOP_AT_SAFE_PLACE,
		/** @since 0.8 */
		S_INSTRUCTION_STOP_AT_SERVICE_AREA,
		/** @since 0.8 */
		S_INSTRUCTION_SWITCH_OFF_ENGINE,
		/** @since 0.8 */
		S_INSTRUCTION_SWITCH_OFF_MOBILE_PHONES,
		/** @since 0.8 */
		S_INSTRUCTION_TEST_BRAKES,
		/** @since 0.8 */
		S_INSTRUCTION_USE_BUS,
		/** @since 0.8 */
		S_INSTRUCTION_USE_FOG_LIGHTS,
		/** @since 0.8 */
		S_INSTRUCTION_USE_HAZARD_LIGHTS,
		/** @since 0.8 */
		S_INSTRUCTION_USE_HEADLIGHTS,
		/** @since 0.8 */
		S_INSTRUCTION_USE_RAIL,
		/** @since 0.8 */
		S_INSTRUCTION_USE_TRAM,
		/** @since 0.8 */
		S_INSTRUCTION_USE_UNDERGROUND,
		/** @since 0.8 */
		S_LANE_USAGE_KEEP_LEFT,
		/** @since 0.8 */
		S_LANE_USAGE_KEEP_RIGHT,
		/** @since 0.8 */
		S_LANE_USAGE_USE_SHOULDER,
		/** @since 0.8 */
		S_PLACE_BEND,
		S_PLACE_BRIDGE,
		/** @since 0.8 */
		S_PLACE_CITY_CENTER,
		/** @since 0.8 */
		S_PLACE_HIGH_ALTITUDE,
		/** @since 0.8 */
		S_PLACE_HILL,
		/** @since 0.8 */
		S_PLACE_INNER_CITY,
		/** @since 0.8 */
		S_PLACE_LOW_AREA,
		S_PLACE_RAMP,
		S_PLACE_ROADWORKS,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_PLACE_SHADE,
		S_PLACE_TUNNEL,
		/** @since 0.8 */
		S_PLACE_TUNNEL_PORTAL,
		/** @since 0.8 */
		S_QUALIFIER_BORDER_ENTRY,
		/** @since 0.8 */
		S_QUALIFIER_BORDER_EXIT,
		/** @since 0.8 */
		S_QUALIFIER_UNTIL_FURTHER_NOTICE,
		/** @since 0.8 */
		S_REASON_HOLIDAY_TRAFFIC,
		/** @since 0.8 */
		S_REASON_TECHNICAL_PROBLEMS,
		/** @since 0.8 */
		S_REASON_TRAFFIC,
		/** @since 0.8 */
		S_REASON_VISITOR_NUMBER,
		/** @since 0.8 */
		S_SPEED_CHECKS,
		/** @since 0.8 */
		S_SPEED_OBSERVE_LIMITS,
		/** @since 0.8 */
		S_SPEED_REDUCE,
		S_TENDENCY_QUEUE_DECREASING,
		S_TENDENCY_QUEUE_INCREASING,
		/** @since 0.8 */
		S_TRAFFIC_ACCESS_ONLY,
		/** @since 0.8 */
		S_TRAFFIC_ARRIVALS,
		/** @since 0.8 */
		S_TRAFFIC_DEPARTURES,
		/** @since 0.8 */
		S_TRAFFIC_FERRY,
		/** @since 0.8 */
		S_TRAFFIC_HOLIDAY,
		/** @since 0.8 */
		S_TRAFFIC_LOCAL,
		/** @since 0.8 */
		S_TRAFFIC_LONG_DISTANCE,
		/** @since 0.8 */
		S_TRAFFIC_RAIL,
		/** @since 0.8 */
		S_TRAFFIC_REGIONAL,
		/** @since 0.8 */
		S_TRAFFIC_RESIDENTS,
		/** @since 0.8 */
		S_TRAFFIC_THROUGH,
		/** @since 0.8 */
		S_TRAFFIC_VISITORS,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_4WD_WITH_SNOW_TIRES_OR_CHAINS,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_ABNORMAL_LOAD,
		S_VEHICLE_ALL,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_ARTICULATED,
		S_VEHICLE_BUS,
		S_VEHICLE_CAR,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_CAR_AND_LIGHT,
		S_VEHICLE_CAR_WITH_CARAVAN,
		S_VEHICLE_CAR_WITH_TRAILER,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_DIESEL,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_EVEN_PLATE,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_EXCEPTIONAL_LOAD,
		/** @deprecated Use {@link #S_TRAFFIC_FERRY} instead. */
		@Deprecated
		S_VEHICLE_FERRY,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_GASOLINE,
		S_VEHICLE_HAZMAT,
		/** @since 0.8 */
		S_VEHICLE_HEAVY,
		S_VEHICLE_HGV,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_HIGH_SIDED,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_LIGHT,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_LPG,
		S_VEHICLE_MOTOR,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_ODD_PLATE,
		/** @deprecated Use {@link #S_TRAFFIC_RAIL} instead. */
		@Deprecated
		S_VEHICLE_RAIL,
		/** @deprecated Use {@link #S_TRAFFIC_THROUGH} instead. */
		@Deprecated
		S_VEHICLE_THROUGH_TRAFFIC,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_UNDERGROUND,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_WITH_CAT,
		S_VEHICLE_WITH_TRAILER,
		/** @deprecated Reserved for future use. */
		@Deprecated
		S_VEHICLE_WITHOUT_CAT,
		/** @since 0.8 */
		S_WARNING_RADIATION,
		/** @since 0.8 */
		S_WARNING_TOXIC_LEAK,
		/** @since 0.8 */
		S_WINTER_CLOSURE
	}

	/**
	 * The supplementary information class.
	 */
	@Attribute(name="class")
	public final Class siClass;

	/**
	 * The supplementary information type.
	 * 
	 * <p>The type can be mapped to a string to be displayed to the user.
	 */
	@Attribute(required=false, name="type")
	public final Type type;

	/**
	 * Quantifier for the supplementary information item.
	 * 
	 * <p>Permissible data types and their meanings depend on {@code type}.
	 */
	@Transient
	public final Quantifier quantifier;

	public TraffSupplementaryInfo(Class siClass, Type type, Quantifier quantifier) {
		super();
		if (siClass == null)
			throw new IllegalArgumentException("supplementary information class must be specified");
		if (type == null)
			throw new IllegalArgumentException("supplementary information type must be specified");
		// TODO check if the SI type supports the quantifier type
		this.siClass = siClass;
		this.type = type;
		this.quantifier = quantifier;
	}

	/**
	 * Instantiates a new {@link TraffSupplementaryInfo} without a quantifier.
	 * 
	 * @param siClass Value for {@link #siClass}
	 * @param type Value for {@link #type}
	 */
	public TraffSupplementaryInfo(Class siClass, Type type) {
		this(siClass, type, null);
	}

	/**
	 * Constructor for XML deserialization.
	 * 
	 * @param siClass
	 * @param type
	 * @param qDimension
	 * @param qDuration
	 * @param qFrequency
	 * @param qInt
	 * @param qInts
	 * @param qPercent
	 * @param qSpeed
	 * @param qTemperature
	 * @param qTime
	 * @param qWeight
	 * @throws ParseException if an invalid quantifier is passed
	 */
	public TraffSupplementaryInfo(@Attribute(name="class") Class siClass,
			@Attribute(name="type") Type type,
			@Attribute(name="q_dimension") Float qDimension,
			@Attribute(name="q_duration") String qDuration,
			@Attribute(name="q_frequency") Float qFrequency,
			@Attribute(name="q_int") Integer qInt,
			@Attribute(name="q_ints") String qInts,
			@Attribute(name="q_percent") Integer qPercent,
			@Attribute(name="q_speed") Integer qSpeed,
			@Attribute(name="q_temperature") Integer qTemperature,
			@Attribute(name="q_time") String qTime,
			@Attribute(name="q_weight") Float qWeight) throws ParseException {
		super();
		if (siClass == null)
			throw new IllegalArgumentException("supplementary information class must be specified");
		if (type == null)
			throw new IllegalArgumentException("supplementary information type must be specified");
		// TODO check if the SI type supports the quantifier type
		this.siClass = siClass;
		this.type = type;
		// FIXME parse quantifiers with units properly
		if (qDimension != null)
			this.quantifier = new DimensionQuantifier(qDimension);
		// TODO qDuration
		else if (qFrequency != null)
			this.quantifier = new FrequencyQuantifier(qFrequency);
		else if (qInt != null)
			this.quantifier = new IntQuantifier(qInt);
		// TODO qInts
		else if (qPercent != null)
			this.quantifier = new PercentQuantifier(qPercent);
		else if (qSpeed != null)
			this.quantifier = new SpeedQuantifier(qSpeed);
		else if (qTemperature != null)
			this.quantifier = new TemperatureQuantifier(qTemperature);
		else if (qTime != null)
			this.quantifier = new TimeQuantifier(Traff.getIsoDateFormat().parse(qTime));
		else if (qWeight != null)
			this.quantifier = new WeightQuantifier(qWeight);
		else
			this.quantifier = null;
	}

	@Attribute(required=false, name="q_dimension")
	public Float getDimensionQuantifier() {
		if (quantifier instanceof DimensionQuantifier)
			return ((DimensionQuantifier) quantifier).value;
		else
			return null;
	}

	@Attribute(required=false, name="q_duration")
	public String getDurationQuantifier() {
		if (quantifier instanceof DurationQuantifier)
			return ((DurationQuantifier) quantifier).toString();
		else
			return null;
	}

	@Attribute(required=false, name="q_frequency")
	public Float getFrequencyQuantifier() {
		if (quantifier instanceof FrequencyQuantifier)
			return ((FrequencyQuantifier) quantifier).frequency;
		else
			return null;
	}

	@Attribute(required=false, name="q_int")
	public Integer getIntQuantifier() {
		if (quantifier instanceof IntQuantifier)
			return ((IntQuantifier) quantifier).value;
		else
			return null;
	}

	@Attribute(required=false, name="q_ints")
	public String getIntsQuantifier() {
		if (quantifier instanceof IntsQuantifier)
			return ((IntsQuantifier) quantifier).toString();
		else
			return null;
	}

	@Attribute(required=false, name="q_percent")
	public Integer getPercentQuantifier() {
		if (quantifier instanceof PercentQuantifier)
			return ((PercentQuantifier) quantifier).percent;
		else
			return null;
	}

	@Attribute(required=false, name="q_speed")
	public Integer getSpeedQuantifier() {
		if (quantifier instanceof SpeedQuantifier)
			return ((SpeedQuantifier) quantifier).speed;
		else
			return null;
	}

	@Attribute(required=false, name="q_temperature")
	public Integer getTemperatureQuantifier() {
		if (quantifier instanceof TemperatureQuantifier)
			return ((TemperatureQuantifier) quantifier).degreesCelsius;
		else
			return null;
	}

	@Attribute(required=false, name="q_time")
	public String getTimeQuantifier() {
		if (quantifier instanceof TimeQuantifier)
			return Traff.getIsoDateFormat().format(((TimeQuantifier) quantifier).time);
		else
			return null;
	}

	@Attribute(required=false, name="q_weight")
	public Float getWeightQuantifier() {
		if (quantifier instanceof WeightQuantifier)
			return ((WeightQuantifier) quantifier).weight;
		else
			return null;
	}

	/**
	 * Returns the supplementary information item in TraFF XML format.
	 * 
	 * <p>Calling this method is equivalent to calling {@link #toXml(int)} with a zero argument. That is, the
	 * root element will not be indented. 
	 */
	@Deprecated
	public String toXml() {
		return toXml(0);
	}

	/**
	 * Returns the supplementary information item in TraFF XML format.
	 * 
	 * @param indent Indentation for the root element
	 */
	@Deprecated
	public String toXml(int indent) {
		String indentStr = Traff.getIndentString(indent);
		StringBuilder builder = new StringBuilder();

		/* tag with attributes */
		builder.append(indentStr).append("<supplementary_info");
		if (this.siClass != null)
			builder.append(String.format(" class=\"%s\"", this.siClass));
		if (this.type != null)
			builder.append(String.format(" type=\"%s\"", this.type));
		if (quantifier != null)
			builder.append(quantifier.toAttribute());
		builder.append("/>\n");
		return builder.toString();
	}
}
