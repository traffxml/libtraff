/*
 * Copyright © 2017–2020 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff.input;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.traff.TraffMessage;

/**
 * Generic superclass for a source of traffic data.
 */
public abstract class DataSource {
	/**
	 * The logger for log output.
	 * 
	 * Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	/* Do not use the MethodHandles hack here, in order to support Android */
	static final Logger LOG = LoggerFactory.getLogger(DataSource.class);

	/**
	 * Property key prefix for individual sources.
	 */
	static final String KEY_SOURCE_PREFIX = "source";

	protected final String id;
	protected final String url;
	protected final Properties properties;

	/**
	 * Returns the ID with which this source identifies itself.
	 * 
	 * <p>This is identical to the prefix the source uses in TraFF message IDs.
	 */
	public String getId() {
		return id;
	}


	/**
	 * Returns the timestamp of the last update.
	 * 
	 * <p>This does not necessarily coincide with the time at which {@link #poll(Collection, int)}
	 * was last invoked: if the source reports a timestamp for its last update, then that timestamp is used.
	 * 
	 * <p>Implementations should ensure that any timestamp used for this method increases periodically, even if
	 * none of the messages have changed. If the source supplies no such timestamp, local time should be used
	 * and updated whenever {@link #poll(Collection, int)} is called.
	 * 
	 * @return The timestamp of the last update, or null if no data has been retrieved from the source
	 */
	public abstract Date getLastUpdate();

	/**
	 * Returns the update interval for the source.
	 * 
	 * <p>This is the interval at which the source provides updated data. Polling more frequently than that will
	 * only yield redundant data and should be avoided.
	 * 
	 * <p>A value of zero indicates that the source may update events at any time.
	 * 
	 * @return The update interval in seconds, 0 if not known or not applicable.
	 */
	public abstract int getMinUpdateInterval();

	// TODO method for last actual update/next expected update?

	/**
	 * Generates a property key for the source.
	 * 
	 * <p>The key is a string of the form {@code source[org.example.source].prop}, where
	 * {@code org.example.source} is replaced with the ID of the source, and {@code prop} is replaced with
	 * the {@code prop} argument.
	 * 
	 * @param prop
	 */
	public String getPropertyKey(String prop) {
		return String.format("%s[%s].%s", KEY_SOURCE_PREFIX, id, prop);
	}

	/**
	 * Returns the URL for this source.
	 */
	public URL getUrl() {
		try {
			return new URL(url);
		} catch (MalformedURLException e) {
			LOG.debug("{}", e);
		}
		return null;
	}

	/**
	 * Whether the source needs to be supplied the set of existing messages with a poll operation.
	 * 
	 * <p>Some sources internally provide just a “snapshot” of the current situation. If a message
	 * previously received from the source is no longer valid, it is simply not sent in any future
	 * feeds. TraFF, on the contrary, requires explicit cancellation of messages which are no
	 * longer valid but have not yet expired. In order to achieve this, such a source must compare
	 * its new messages against the set of currently known messages, and generate cancellations for
	 * those which have disappeared.
	 * 
	 * <p>The return value from this method indicates if the source requires this. If this method
	 * returns true, a set of all currently valid messages from the source must be passed to the
	 * {@link #poll(Collection, int)} method. Otherwise null can be passed instead.
	 * 
	 * @return Whether the set of existing messages must be supplied along with a poll operation.
	 */
	public abstract boolean needsExistingMessages();

	/**
	 * Retrieves a list of messages from the source.
	 * 
	 * <p>Sources should return null to indicate failure, or an empty list there are no messages to report.
	 * 
	 * <p>This method allows previously received messages to be specified as an optional argument, in order for
	 * information from those messages to be used in the feed. Sources must ensure that messages do not
	 * simply “disappear” from one feed to the next, as this will cause the old message to remain cached
	 * until it expires. If this is not desired, sources should compare the new feed to the collection of old
	 * messages supplied, and generate cancellations for every message which is no longer valid and not
	 * replaced by a new message.
	 * 
	 * <p>The {@code pollInterval} argument can be used by the source to adjust message expiration times,
	 * ensuring messages will not expire before the next update is received. Sources should avoid returning
	 * messages which are set to expire before an update can be retrieved, unless the condition is known to
	 * end before that time.
	 * 
	 * @param oldMessages Previously received messages, can be null
	 * @param pollInterval The interval at which the source will be polled, in seconds
	 */
	// TODO how do we determine if that is a full list or just a difference over the last poll?
	public abstract Collection<TraffMessage> poll(Collection<TraffMessage> oldMessages, int pollInterval);

	public DataSource(String id, String url, Properties properties) {
		this.id = id;
		this.url = url;
		this.properties = properties;
	}
}
