/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.traff;

/**
 * Specifies a list of integer values.
 */
public class IntsQuantifier extends Quantifier {
	/** The quantifier type for use in XML. */
	public final String type = "q_ints";

	public final int[] values;

	public IntsQuantifier(int[] values) {
		super();
		this.values = values;
	}

	/**
	 * Returns the quantifier formatted as an XML attribute.
	 */
	@Deprecated
	public String toAttribute() {
		StringBuilder builder = new StringBuilder();
		builder.append(" ").append(type).append("=\"");
		builder.append(this.toString());
		builder.append('"');
		return builder.toString();
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < values.length; i++)
			builder.append(values[i]).append(i < (values.length - 1) ? ",":"");
		return builder.toString();
	}
}
