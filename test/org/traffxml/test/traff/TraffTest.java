package org.traffxml.test.traff;
/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-eismoinfo-intensity library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.IntQuantifier;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffFeed;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffLocation.Directionality;
import org.traffxml.traff.TraffLocation.RoadClass;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.TraffSupplementaryInfo;
import org.traffxml.traff.subscription.FilterItem;
import org.traffxml.traff.subscription.FilterList;
import org.traffxml.traff.subscription.TraffRequest;
import org.traffxml.traff.subscription.TraffRequest.Operation;
import org.traffxml.traff.subscription.TraffResponse;
import org.traffxml.traff.subscription.TraffResponse.Status;

public class TraffTest {
	private static final int FLAG_GENERATE = 0x1;
	private static final int FLAG_GENERATE_REQ = 0x8;
	private static final int FLAG_GENERATE_RES = 0x20;
	private static final int FLAG_GENERATE_FLT = 0x80;
	private static final int FLAG_READ = 0x2;
	private static final int FLAG_READ_MESSAGE = 0x4;
	private static final int FLAG_READ_REQ = 0x10;
	private static final int FLAG_READ_RES = 0x40;
	private static final int FLAG_READ_FLT = 0x100;
	// private static final int FLAG_INTERNAL = 0x10000; // example of a flag that can be applied on top of the main operation
	private static final int MASK_OPERATIONS = 0xFFFF;

	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs an argument.");
			System.exit(1);
		}
		return args[pos];
	}

	private static void printUsage() {
		System.out.println("Arguments:");
		System.out.println("  -generate         Generate a feed");
		System.out.println("  -generatereq      Generate a request");
		System.out.println("  -generateres      Generate a response");
		System.out.println("  -generatefilter   Generate a filter list");
		System.out.println("  -read <feed>      Read a feed");
		System.out.println("  -readmsg <msg>    Read a message");
		System.out.println("  -readreq <req>    Read a request");
		System.out.println("  -readres <res>    Read a response");
		System.out.println("  -readfilter <flt> Read a filter list");
		System.out.println();
		System.out.println("<feed>, <msg>, <req>, <res> and <flt> can refer to a local file or an http/https URL");
	}

	public static void main(String[] args) {
		TraffFeed feed = null;
		TraffMessage message = null;
		TraffRequest request = null;
		TraffResponse response = null;
		FilterList filterList = null;
		String input = null;
		int i;
		int flags = 0;

		if (args.length == 0) {
			printUsage();
			System.exit(1);
		}
		for (i = 0; i < args.length; i++) {
			if ("-generate".equals(args[i])) {
				flags |= FLAG_GENERATE;
			} else if ("-generatereq".equals(args[i])) {
				flags |= FLAG_GENERATE_REQ;
			} else if ("-generateres".equals(args[i])) {
				flags |= FLAG_GENERATE_RES;
			} else if ("-generatefilter".equals(args[i])) {
				flags |= FLAG_GENERATE_FLT;
			} else if ("-read".equals(args[i])) {
				input = getParam("read", args, ++i);
				flags |= FLAG_READ;
			} else if ("-readmsg".equals(args[i])) {
				input = getParam("readmsg", args, ++i);
				flags |= FLAG_READ_MESSAGE;
			} else if ("-readreq".equals(args[i])) {
				input = getParam("readreq", args, ++i);
				flags |= FLAG_READ_REQ;
			} else if ("-readres".equals(args[i])) {
				input = getParam("readres", args, ++i);
				flags |= FLAG_READ_RES;
			} else if ("-readfilter".equals(args[i])) {
				input = getParam("readfilter", args, ++i);
				flags |= FLAG_READ_FLT;
			} else {
				System.out.println("Unknown argument: " + args[i]);
				System.out.println();
				printUsage();
				System.exit(0);
			}
		}
		switch(flags & MASK_OPERATIONS) {
		case FLAG_GENERATE:
			feed = generateFeed();
			break;
		case FLAG_GENERATE_REQ:
			request = generateRequest();
			break;
		case FLAG_GENERATE_RES:
			response = generateResponse();
			break;
		case FLAG_GENERATE_FLT:
			filterList = generateFilter();
			break;
		case FLAG_READ:
			feed = readFeed(input);
			break;
		case FLAG_READ_MESSAGE:
			message = readMessage(input);
			break;
		case FLAG_READ_REQ:
			request = readRequest(input);
			break;
		case FLAG_READ_RES:
			response = readResponse(input);
			break;
		case FLAG_READ_FLT:
			filterList = readFilter(input);
			break;
		default:
			System.err.println("Invalid combination of options. Aborting.");
			System.exit(1);
		}
		// use SimpleXML
		try {
			if (feed != null)
				feed.write(System.out);
			else if (message != null)
				message.write(System.out);
			else if (request != null)
				request.write(System.out);
			else if (response != null)
				response.write(System.out);
			else if (filterList != null)
				filterList.write(System.out);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static TraffFeed generateFeed() {
		TraffFeed res = new TraffFeed();
		Date date1 = new Date(119, 10, 1, 11, 55, 42);
		Date date2 = new Date(119, 10, 1, 12, 00, 42);
		Date date3 = new Date(119, 10, 1, 12, 30, 42);
		TraffMessage.Builder messageBuilder;
		TraffEvent.Builder eventBuilder;
		TraffLocation.Builder locationBuilder;

		messageBuilder = new TraffMessage.Builder();
		messageBuilder.setId("test:simple");
		messageBuilder.setReceiveTime(date1);
		messageBuilder.setUpdateTime(date2);
		messageBuilder.setExpirationTime(date3);
		messageBuilder.addReplaces("test:old");
		locationBuilder = new TraffLocation.Builder();
		locationBuilder.setCountry("LT");
		locationBuilder.setOrigin("Klaipėda");
		locationBuilder.setDestination("Vilnius");
		locationBuilder.setDirectionality(Directionality.ONE_DIRECTION);
		locationBuilder.setRoadClass(RoadClass.MOTORWAY);
		locationBuilder.setRoadRef("A1");
		locationBuilder.setFrom(new TraffLocation.Point(55.707481f, 21.550529f, "Vėžaičiai", null));
		locationBuilder.setTo(new TraffLocation.Point(55.687099f, 21.691055f, "Endriejavas", null));
		messageBuilder.setLocation(locationBuilder.build());
		eventBuilder = new TraffEvent.Builder();
		eventBuilder.setEventClass(TraffEvent.Class.RESTRICTION);
		eventBuilder.setType(TraffEvent.Type.RESTRICTION_LANE_BLOCKED);
		eventBuilder.setQuantifier(new IntQuantifier(1));
		messageBuilder.addEvent(eventBuilder.build());
		eventBuilder = new TraffEvent.Builder();
		eventBuilder.setEventClass(TraffEvent.Class.CONGESTION);
		eventBuilder.setType(TraffEvent.Type.CONGESTION_TRAFFIC_PROBLEM);
		eventBuilder.setSpeed(50);
		eventBuilder.addSupplementaryInfo(new TraffSupplementaryInfo(TraffSupplementaryInfo.Class.TENDENCY,
				TraffSupplementaryInfo.Type.S_TENDENCY_QUEUE_DECREASING));
		messageBuilder.addEvent(eventBuilder.build());
		res.getMessages().add(messageBuilder.build());

		// TODO generate more messages for feed
		return res;
	}

	private static FilterList generateFilter() {
		List<FilterItem> filterList = new ArrayList<FilterItem>();
		filterList.add(new FilterItem(new BoundingBox(45.0f, 9.0f, 46.0f, 10.0f), null));
		filterList.add(new FilterItem(new BoundingBox(45.0f, 9.0f, 49.0f, 12.0f), RoadClass.PRIMARY));
		filterList.add(new FilterItem(new BoundingBox(48.0f, 11.0f, 49.0f, 12.0f), null));
		return new FilterList(filterList);
	}

	private static TraffRequest generateRequest() {
		List<FilterItem> filterList = new ArrayList<FilterItem>();
		filterList.add(new FilterItem(new BoundingBox(45.0f, 9.0f, 46.0f, 10.0f), null));
		filterList.add(new FilterItem(new BoundingBox(45.0f, 9.0f, 49.0f, 12.0f), RoadClass.PRIMARY));
		filterList.add(new FilterItem(new BoundingBox(48.0f, 11.0f, 49.0f, 12.0f), null));
		return new TraffRequest(Operation.CHANGE, "deadbeef", filterList);
	}

	private static TraffResponse generateResponse() {
		TraffFeed feed = generateFeed();
		return new TraffResponse(Status.OK, "deadbeef", null, null, feed);
	}

	private static InputStream openInputStream(String input) {
		InputStream res = null;
		if ((input == null) || (input.isEmpty())) {
			System.err.println("Input file or URL must be specified.");
			System.exit(1);
		}
		if (input.startsWith("http://") || input.startsWith("https://")) {
			URL url;
			try {
				url = new URL(input);
				res = url.openStream();
			} catch (MalformedURLException e) {
				System.err.println("Input URL is invalid. Aborting.");
				System.exit(1);
			} catch (IOException e) {
				System.err.println("Cannot read from input URL. Aborting.");
				System.exit(1);
			}
		} else {
			File inputFile = new File(input);
			if (!inputFile.exists()) {
				System.err.println("Input file does not exist. Aborting.");
				System.exit(1);
			} else if (!inputFile.isFile()) {
				System.err.println("Input file is not a file. Aborting.");
				System.exit(1);
			} else if (!inputFile.canRead()) {
				System.err.println("Input file is not readable. Aborting.");
				System.exit(1);
			}
			try {
				res = new FileInputStream(inputFile);
			} catch (FileNotFoundException e) {
				System.err.println("Input file does not exist. Aborting.");
				System.exit(1);
			}
		}
		return res;
	}

	private static TraffFeed readFeed(String input) {
		TraffFeed res = new TraffFeed();
		InputStream dataInputStream = null;
		dataInputStream = openInputStream(input);

		if (dataInputStream != null) {
			try {
				res = TraffFeed.read(dataInputStream);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}
		return res;
	}

	private static FilterList readFilter(String input) {
		FilterList res = null;
		InputStream dataInputStream = null;
		dataInputStream = openInputStream(input);

		if (dataInputStream != null) {
			try {
				res = FilterList.read(dataInputStream);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}
		return res;
	}

	private static TraffMessage readMessage(String input) {
		TraffMessage res = null;
		InputStream dataInputStream = null;
		dataInputStream = openInputStream(input);

		if (dataInputStream != null) {
			try {
				res = TraffMessage.read(dataInputStream);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}
		return res;
	}

	private static TraffRequest readRequest(String input) {
		TraffRequest res = null;
		InputStream dataInputStream = null;
		dataInputStream = openInputStream(input);

		if (dataInputStream != null) {
			try {
				res = TraffRequest.read(dataInputStream);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}
		return res;
	}

	private static TraffResponse readResponse(String input) {
		TraffResponse res = null;
		InputStream dataInputStream = null;
		dataInputStream = openInputStream(input);

		if (dataInputStream != null) {
			try {
				res = TraffResponse.read(dataInputStream);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}
		return res;
	}
}
